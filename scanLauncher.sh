#!/bin/bash
#set -x

########################################
# Author : Shohei Yamagaya(Osaka univ.)
########################################

function wrapAndExit(){
    exit 1
}

trap 'wrapAndExit' 2

function usage(){
    cat <<EOF
$0
Help:
  -m <str>      : Serial number of module (Req *)
  -s <str/path> : scan type               (Req *)
  -d <str/path> : enable the DCS functinality
  -t            : Sync DCS data from InfluxDB to LocalDB after each scan.
  -W            : upload Yarr's results to localDB
  -Q            : enable file valification and interactive features for QC testing.
  -h            : Show this
EOF
    exit 1
}

function check_value(){
    if [[ "$2" == "" ]] || [[ $2 = -* ]];then
        echo "[ error ][sl] option \"$1\" requires a parameter!"
	    usage
    fi
}

# A few colors for echoing
function darkblue(){
    echo "\033[0;34m$1\033[0m"
}
function green(){
    echo "\033[0;32m$1\033[0m"
}

###### default ##############################
dcsMonitor=0
syncDatabases=0
dbComConfig=""
localdbOpt=""
#############################################




while getopts m:s:tdWQh OPT
do
    case $OPT in
    m) module_id=$OPTARG ;;
    s) scan=$OPTARG ;;
    d) dcsMonitor=1 ;;
    t) syncDatabases=1 ;;
    W) localdbOpt="-W" ;;
    Q) qcOpt="-Q" ;;
    h) usage;;
    *) usage ;;
    esac
done

# Check if these oprions where properly supplied
check_value "-m" $module_id
check_value "-s" $scan
# variables from ScanOperator.sh is temporary here
# if [[ "$SOBASE" == "" ]]; then
if [[ "$SOBASE" == "" ]] || [[ $ParaOpt != "" ]] ; then
    # Two important constants here
    export SOBASE=$(cd $(dirname $0); pwd)
    INDEXJSON="$SOBASE/configs/so_index.json"
    if [ ! -f $INDEXJSON ];then
        echo -e "$(red [\ error\ ][so]) Not found file \"$INDEXJSON\""
        echo -e "$(red [\ error\ ][so]) Put the file back there and try again."
        usage
        exit
    fi
    # Config files used here and there
    MODULECFG="$SOBASE/`jq -j '.module_config' $INDEXJSON`"
    INFLUXDBENVCFG="$SOBASE/`jq -j '.influxdb_config_env' $INDEXJSON`"
    #Check whether the module exists in $MODULECFG.
    value="$(cat $MODULECFG | jq --arg a $module_id '.modules."\($a)"')"
    if [ "$value" = "null" ]; then
        echo -e "$(red [\ error\ ][so]) No module ID \"$module_id\" found under the entry \"modules\" in $MODULECFG."
        echo "[ info  ][so] Only the following module IDs are currently defined in the config file:"

        jq -r '.modules | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG |
        while IFS=" " read -r -a value; do
            key=${value[0]}
            echo "-   $key"
        done

        echo "[ info  ][so] Please add yours (or rename one of the above) and try again."
        exit
    fi
    #Check RD53A or RD53B
    export RD53XL=$(jq -j --arg a $module_id '.modules."\($a)".chipType' $MODULECFG)
    export RD53XS=${RD53XL,,}
    ScanFlow=${RD53XS}_scans_config
    export INFLUXDBDCSCFG="$SOBASE/`jq -j '.influxdb_config_dcs' $INDEXJSON`"
    export SCANCFG="$SOBASE/$(jq --arg a $ScanFlow -j '."\($a)"' $INDEXJSON)"
    export DBCOMCFG="$SOBASE/`jq -j '.dbs_communication_config' $INDEXJSON`"
    export PSCFG="$SOBASE/`jq -j '.powersupply_config' $INDEXJSON`"
    export LDBUSRCFG="$SOBASE/`jq -j '.localdb_user_config' $INDEXJSON`"
    export LDBSITCFG="$SOBASE/`jq -j '.localdb_site_config' $INDEXJSON`"

    export YARRBASE=`jq -j '.YARR_directory' $SCANCFG`

    #Check whether the module exists in $SCANCFG.

    if [ -z $module_id ];then
        echo "Please specify a Module ID (option '-m'). "
        usage
    fi
    if [ ! -d "$YARRBASE" ]; then
        echo -e "$(red [\ error\ ][so]) Please specify a valid path to Yarr under \n" \
            "             \".YARR_directory\" in \"$SCANCFG\""
        exit 1
    fi

fi

localdbConf=$(find $HOME/.yarr/localdb/ -type f -iname "*_database.json")

if [[ $syncDatabases == 1 ]];
then
    jq -r  '.environment | keys[] as $k | "\($k) \(.[$k])"' $DBCOMCFG |
    while IFS=" " read -r -a value; do
        key=${value[0]}
        val=${value[1]}

        if [ "$(jq --arg v $val  '.environment | index( "\($v)" )' $localdbConf)" == "null" ]
        then
            jq --arg v $val  '.environment += ["\($v)"]' $localdbConf > tmp && mv tmp $localdbConf
            echo -e "[ warn ][so] Added $(darkblue \"$val\") to \".environment\" in $localdbConf"
        fi

    done
fi

#Get other option!
addOpts="$(echo "${scan}" | jq -r '.[1]')"
#echo $addOpts
addOpts="$(echo "${addOpts}" | sed -e "s/thisIsSpace/\ /g")"
#scan="$(echo -e "${scan}" | tr -d '[:space:]')"
# common option to YARR from yarr.json
comOpts=$(jq -j '.common_config.default_option' $SCANCFG)
echo $comOpts$

scanName=$(echo $scan | jq -r '.[0]')
scan="$(echo "${scan}" | sed -e "s/thisIsSpace/\ /g")"
echo "scanName=$scanName, scan=$scan"
scanPath="$YARRBASE/configs/scans/$RD53XS/$scanName.json"

# Build Yarr's "-t" option
targetToT=""
targetAmpOrCharge=""
targetOption=""
if echo "${scanName}" | grep "tune_globalpreamp" >>/dev/null; then
    if [[ $(echo $scan | jq '. | length') == 1 ]]; then
        echo "[ error ] Please specify the ToT together with the scan name"
        exit 1
    else
        targetAmpOrCharge=$(jq -j '.common_config.target_preamp' $SCANCFG)
	targetToT=$(echo $scan | jq -r '.[1]')
	targetOption="-t ${targetAmpOrCharge} ${targetToT} "
    fi
elif echo "${scanName}" | grep "totscan" >>/dev/null; then
    targetAmpOrCharge=$(jq -j '.common_config.target_preamp' $SCANCFG)
    targetOption="-t ${targetAmpOrCharge} "
elif [[ $(echo $scan | jq '. | length') == 2 ]]; then
    targetAmpOrCharge=$(echo $scan | jq -r '.[1]')
    targetOption="-t ${targetAmpOrCharge}"
fi

# Build Yarr's "-m" option (reset px masks, default -1 (same as in Yarr))
reset_masks=-1
if echo "${scanName}" | grep "digital\|analog\|noise" >>/dev/null; then

    if [[ $(echo $scan | jq '. | length') == 2 ]]; then
        reset_masks=$(echo $scan | jq -r '.[1]')
    fi

fi

###### Log file  ######################
RunNumber=$(cat $HOME/.yarr/runCounter)
RunNumber=$(( RunNumber +  1))
printf -v RunNumber "%06d" $RunNumber
#export result_data_DIR=$SOBASE"/data/"$RunNumber"_"${scanName}"/"
#mkdir -p $result_data_DIR
#LOGFILE="${result_data_DIR}OperatorLog."`date "+%Y%m%d_%H%M%S"`
###############################################


if [[ $dcsMonitor == 1 ]] ;then
    echo "[ info ][sl] Getting single [V, I] measurement"
    chn=""

    nChannels="$(jq -j '.channels_to_be_used_by_the_scanoperator | length' $PSCFG)"
    for (( i=0; i<$nChannels; i++ ))
    do
        chName="$(jq -j ".channels_to_be_used_by_the_scanoperator[$i].name" $PSCFG)"
        chn+="$chName,"
    done
    chn="${chn::-1}"

    python3 $SOBASE/libDCS/psOperator.py -e $PSCFG -c $chn -d $INFLUXDBDCSCFG measure
    # Tell read_dcs_background.sh that we're performing a scan
    echo "1" > $SOBASE/.counters/dcsStat

    $SOBASE/libDCS/read_dcs_background.sh $PSCFG $INFLUXDBDCSCFG &
fi

#targetCharge=$(jq -j '.scan.target_charge' $SCANCFG)

ctrlFile="$SOBASE/configs/$RD53XS/$module_id/controller.json"
cnctFile="$SOBASE/configs/$RD53XS/$module_id/connectivity.json"

#ctrlFile="/home/mario/work/yr/configs/controller/emuCfg.json"
#comm="unbuffer $YARRBASE/bin/scanConsole -r $ctrlFile -c $cnctFile -p ${targetOption} -s ${scanPath}  -m $reset_masks -o $SOBASE/data/by_runnumber/$SO_RUNNUMBER $localdbOpt $qcOpt -n 1" #> /dev/null 2>1
comm="unbuffer $YARRBASE/bin/scanConsole -r $ctrlFile -c $cnctFile -s ${scanPath} -o $SOBASE/data/by_runnumber/$SO_RUNNUMBER $localdbOpt $qcOpt $addOpts $comOpts" #> /dev/null 2>1
echo "[ info ][sl] Calling Yarr's scanConsole"
echo "[ info ][sl] $comm"

# If Yarr fails, try again from the same scan, a maximum of 3 times.
max_reps=$(jq -j '.common_config.max_trials_in_case_of_failure' $SCANCFG)

# run and time every scan (yarr time)
START_YR=$(date +%s.%N)

reps=0; retval=-1
while [ true ]; do
    ((reps++))

    $comm   # call Yarr
    retval=$?
    if [ $retval -eq 0 ]; then
        break
    fi

    if [ $retval -ne 0 ] && [ $reps -ge $max_reps ]; then 
        echo "[ warn ][sl] After $max_reps attempts, couldn't successfully finish $scanName."
        exit $retval

    elif [ $retval -ne 0 ]; then
        echo "[ warn ][sl] Got an error from Yarr. Trying $scanName again in 3s ..."
        sleep 3
    fi

done

elp=$(echo "$(date +%s.%N) - $START_YR" | bc)
echo -ne "$scanName     $elp\n" >> $TIMEFILE



# stop taking data
if [[ $dcsMonitor == 1 ]] ;then
    echo "[ info ][sl] Stopping data taking"
    echo "0" > $SOBASE/.counters/dcsStat
fi

if [[ "$localdbOpt" != "" ]]; then

    #if [ $dcsMonitor == 1 ] && [ $syncDatabases == 1 ] ;then
    if [ $syncDatabases == 1 ] ;then

        cmd="$YARRBASE/bin/dbAccessor -F $DBCOMCFG -n $module_id -s $SOBASE/data/by_runnumber/$SO_RUNNUMBER/${RunNumber}_$scanName/scanLog.json"
        echo "[ info ][sl] Pushing the DCS data to localDB"
        echo "[ info ][sl] $cmd"
        $cmd
        echo "[ info ][sl] done"

    elif ! [ $syncDatabases == 1 ] ;then

        cmd="$YARRBASE/bin/dbAccessor -F \$1 -n $module_id -s $SOBASE/data/by_runnumber/$SO_RUNNUMBER/${RunNumber}_$scanName/scanLog.json"
        echo "$cmd" >> $SOBASE/data/by_runnumber/$SO_RUNNUMBER/moveDCStoLocalDB.sh

    fi
fi

