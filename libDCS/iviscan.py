import argparse
import signal
import sys, os
import json
import numpy as np
import time
import getpass
from influxdb import InfluxDBClient, exceptions
import logging, coloredlogs, verboselogs

# LR stuff
LABREMOTE = 1
try:
    import labRemote
except:
    try:
        import _labRemote as labRemote
    except:
        LABREMOTE = 0


def sigint_handler(sig, frame):
    logger.info('Received SIGINT. Exiting!')
    sys.exit(0)


signal.signal(signal.SIGINT, sigint_handler)


def powerPassiveChannels(config, key, what):
    
    if what == "turnOn":
        for elem in config[key]["passive_channels"]:
            if elem != {}:

                ps = hw.getPowerSupplyChannel(elem["name"])

                logger.info("setting I to %2g A and V to %2g V on channel %s" % (elem["current"], elem["voltage"], elem["name"]))
                ps.setVoltageLevel(elem["voltage"])
                ps.setCurrentLevel(elem["current"])

                ps.turnOn()

    elif what == "turnOff":
        for elem in config[key]["passive_channels"]:
            if elem != {}:
                ps = hw.getPowerSupplyChannel(elem["name"])
                ps.turnOn()


def ramp(varName, pschannel, target, step = None, tsleep = None):

    if varName == "voltage":

        xVarName = "voltage" ; xUnit = "V"
        yVarName = "current" ; yUnit = "A"
        measXvar = pschannel.measureVoltage
        setXvar = pschannel.setVoltageLevel
        measYvar = pschannel.measureCurrent
        tolerance = 1 # V
        if step   is None: step = 5 # V
        if tsleep is None: tsleep = 1 # s

    elif varName == "current":
        
        xVarName = "current" ; xUnit = "A"
        yVarName = "voltage" ; yUnit = "V"
        measXvar = pschannel.measureCurrent
        setXvar = pschannel.setCurrentLevel
        measYvar = pschannel.measureVoltage
        tolerance = 0.02 # A
        if step   is None: step = 0.1 # A
        if tsleep is None: tsleep = 0.5 # s

    else:
        logger.error("'varName' can only be 'voltage' or 'current'")


    # Get current values of V and I
    xmeas = measXvar()
    ymeas = measYvar()
    #isOn = pschannel.getPowerSupply().isOn(pschannel.getChannel())
    isOn = (measXvar() != 0 or measYvar() != 0)
    
    diff = target - xmeas
    logging.debug(varName + " difference (target - meas): " + str(diff) + " " + xUnit)

    # Decide whether ramping is finished or not
    if abs(diff) <= tolerance / 2:
        logger.info("ramping finished")
        return xmeas


    # determine direction of ramp
    if diff < 0 and step > 0 or diff > 0 and step < 0 :
        step = -step

    logger.info("ramping from %.2g %s to %.2g %s in steps of %.2g %s" % (xmeas, xUnit, target, xUnit, step, xUnit))

    while abs(xmeas) >= abs(target)+abs(step) or abs(xmeas) <= abs(target)-abs(step):
        setXvar(xmeas+step)
        time.sleep(tsleep)
        xmeas = measXvar()
        ymeas = measYvar()
        logger.debug("PSU is on? " + str(isOn) + "; " + xVarName + ": " + str(xmeas) + " " + xUnit + "; " + yVarName + ": " + str(ymeas) + " " + yUnit)
    
    diff = target - xmeas
    
    if abs(diff) < abs(step):
        logging.debug("--------------------")
        logger.info("[%s, %s] = [%.2g %s, %.2g %s] " % (xVarName, yVarName, xmeas, xUnit, ymeas, yUnit))
        ramp(xVarName, pschannel, target, step/5.)    


def doivi(config, w, dbSink=""):

    if w == "iv":
        key = "sensor_iv"
        outputKey = "Sensor_IV"
        xVarName = "voltage"; xKeyOutName = "Voltage"; xKeyOutUnit = "V"
        yVarName = "current"; yKeyOutName = "Current"; yKeyOutUnit = "A"

    elif w == "vi":
        key = "module_vi"
        outputKey = "SLDO_VI"
        xVarName = "current"; xKeyOutName = "Current"; xKeyOutUnit = "A"
        yVarName = "voltage"; yKeyOutName = "Voltage"; yKeyOutUnit = "V"


    ch = config[key]["target_channel"]
    ps = hw.getPowerSupplyChannel(ch["name"])
    if ps == None:
        logger.error("change the config and try again!")
        exit(1)

    if w == "iv":
        setXvar = ps.setVoltageLevel
        getXvar = ps.getVoltageLevel
        measXvar = ps.measureVoltage
        measYvar = ps.measureCurrent

    elif w == "vi":
        setXvar = ps.setCurrentLevel
        getXvar = ps.getCurrentLevel
        measXvar = ps.measureCurrent
        measYvar = ps.measureVoltage


    logger.info("Starting the %s scan..." % w)

    if w == "iv": 
        ps.setCurrentProtect(ch["current_protection"])
        
    elif w == "vi":
        ps.setVoltageProtect(ch["voltage_protection"]) 
        ps.setVoltageLevel(ch["setting_voltage"])


    start = time.time()

    powerPassiveChannels(config, key, "turnOn")


    # output as JSON
    # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > 
    data = {}

    data['unit'] = {}
    data['unit']['Time'] = 's'
    data['unit'][xKeyOutName] = xKeyOutUnit
    data['unit'][yKeyOutName] = yKeyOutUnit

    data[xKeyOutUnit+"_step"]       = config[key]["target_channel"]["step_size"]
    data["Step_duration"]           = config[key]["target_channel"]["sleeping_time_per_step_s"]
    data["N_measurements_per_step"] = config[key]["target_channel"]["measurements_per_step"]
    data[yKeyOutUnit+"_Compliance"] = config[key]["target_channel"][yVarName+"_protection"]

    data[outputKey] = []


    xVar = ch["initial_" + xVarName]
    step = ch["step_size"]

    startX = measXvar()
    #wasOn = ps.isOn()
    wasOn = (measXvar() != 0 or measYvar() != 0)

    # Ramp to the starting point if PSU is already on
    if wasOn:
        ramp(xVarName, ps, xVar)#, 5, 1)
    else:
        startX = getXvar()
        setXvar(xVar)
        ps.turnOn()

    # Output to the terminal
    # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > 
    colWidths = "{0:8}{1:17}{2:20}{3:20}{4:20}{5:20}"
    print(colWidths.format("# time", xVarName + "_Level", xVarName + "_SenseMean", xVarName + "_SenseStd", yVarName + "_SenseMean", yVarName + "_SenseStd"))
    # < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < 

    while (xVar - ch["final_" + xVarName])* step/abs(step) <= 0:

        setXvar(xVar)
        if w == "vi":
            ps.turnOn()
        
        time.sleep(ch["sleeping_time_per_step_s"])

        xVarPerStep = []
        yVarPerStep = []

        for j in range(ch["measurements_per_step"]):
            time.sleep(0.1)
            measX = measXvar()
            measY = measYvar()

            xVarPerStep.append(measX)
            yVarPerStep.append(measY)

            if measY > ch[yVarName + "_protection"]:
                logger.warning("Reached " + yVarName + " protection value")
                break
        else:

            elapsedTime = time.time() - start

            xVarMean = np.mean(xVarPerStep)
            xVarStd  = np.std(xVarPerStep)
            yVarMean = np.mean(yVarPerStep)
            yVarStd  = np.std(yVarPerStep)

            data[outputKey].append({"Time" : elapsedTime})
            data[outputKey][-1][xKeyOutName] = xVarMean
            data[outputKey][-1][yKeyOutName+"_mean"] = yVarMean
            data[outputKey][-1][yKeyOutName+"_sigma"] = yVarStd

            # Terminal output
            print(colWidths.format("%.2g" % elapsedTime, "%.3g" % xVar, "%.3g" % xVarMean, "%.3g" % xVarStd, "%.3g" % yVarMean, "%.3g" % yVarStd))


            # Output to InfluxDB
            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > 
            if dbSink != "":
                point = {
                    "measurement": dbSink["measurement"],
                    "fields": {
                        "x_" + xVarName + "Setting": xVar,
                        "x_" + xVarName + "SenseMean": xVarMean,
                        "x_" + xVarName + "SenseStd": xVarStd,
                        "y_" + yVarName + "SenseMean": yVarMean,
                        "y_" + yVarName + "SenseStd": yVarStd
                    },
                    "tags": {
                        "user": getpass.getuser()
                    }
                }
                toInfluxDB([point], dbSink)
            # < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < 

            xVar += step

            # turn everything off (power-cycle the PS)
            if w == "vi":
                ps.turnOff()

            continue
        break

    with open(args.outfile+"_"+w+".json", "w") as f:
        json.dump(data, f, indent=4)
        
    if w == "iv":
        # ramp HV outside of IV curve according to module testing document
        # 2V/s for 3D, 5V/s for planar
        ramp(xVarName, ps, startX)#, 5, 1)
        if not wasOn:
            ramp(xVarName, ps, 0.0)
            ps.turnOff()

    if w == "vi":
        setXvar(startX)
        if wasOn:
            ps.turnOn()

    powerPassiveChannels(config, key, "turnOff")



    


def toInfluxDB(points, dbSink, host='localhost', port=8086):

    host    = dbSink["host"]
    port    = dbSink["port"]
    dbName  = dbSink["database"]
    usr     = dbSink["username"]
    pwd     = ""

    if "INFLUXDBPWD" in  os.environ:
        # Don't ask the user twice (if the pwd was already entered in a previous stage)
        pwd = str(os.environ['INFLUXDBPWD'])

    try:
        # Try to connect without authentication
        client = InfluxDBClient(host=host,
                                port=port,
                                database=dbName,
                                username=usr,
                                password=pwd)

        # Check if it worked
        database_list=client.get_list_database()
        
    except exceptions.InfluxDBClientError as e:

        # If auth is enabled, ask the user for his/her password
        if "authorization failed" in str(e):
            logger.warning("Input the password for user \"{}\" in InfluxDB.\n" 
                  "         If you don\'t want to input the password in the future until\n"
                  "         you close your current bash session use \n"
                  "                  \"export INFLUXDBPWD=yourPassword\".".format(usr))
            try:
                pwd = getpass.getpass()
            except Exception as e:
                logger.error(e)
                sys.exit(1)

    # And connect again.
    client=InfluxDBClient(host=host,
                          port=port,
                          database=dbName,
                          username=usr,
                          password=pwd)

    try:
        # This should be working now if the user entered the right pwd
        database_list=client.get_list_database()

    except exceptions.InfluxDBClientError as e:

        # If not, let the user know what's happening and exit.
        logger.error("Received error from InfluxDB: {}".format(e))
        logger.info("Please specify the db connectivity parameters "
                "\"database\", and also \"username\", " 
                "\"host\" and \"port\" in the .\"influxdb_cfg\" section in" 
                "the provided DCS config file. Make sure the provided username and password are correct.\n")

        sys.exit(1)

    try:
        client.write_points(points)
    except exceptions.InfluxDBClientError as e:
        logger.error("Received error from InfluxDB: {}".format(e))
        sys.exit(1)


if __name__ == "__main__":
    
    coloredlogs.install(level="INFO") ### controls availability of colored logs
    logger = verboselogs.VerboseLogger('ivi')


    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-w",
        "--what",
        required=True,
        help="'iv' for IV curve, 'vi' for VI curve.",
    )
    parser.add_argument(
        "-j",
        "--json",
        required=False,
        default="configs/lr_iviscan.json",
        help="path to iviscan.json. Default is \"configs/lr_iviscan.json\"",
    )
    parser.add_argument(
        "-e",
        "--equip",
        required=True,
        help="powersupply.json Configuration file with the power supply definition",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        required=False,
        default="output",
        help="output file name without file extension. Will be saved in .json format. Default is \"output\"",
    )
    parser.add_argument(
        "-u",
        "--upload",
        required=False,
        action="store_true",
        help="If set will upload data to influxDB. \"influxdb_cfg\" required in powersupply.json",
    )
    
    args = parser.parse_args()
    logger.debug(args)

    # - - - - - - - - - -
    with open(args.json) as f:
        config = json.load(f)
    
    with open(args.equip) as g:
        psConfig = json.load(g)


    if LABREMOTE == 0:

        with open(args.equip) as f:
            lrLibPath = json.load(f)["LR_lib_path"]
        
        sys.path.append(lrLibPath)

        try:
            import labRemote
        except:
            try:
                import _labRemote as labRemote
            except:
                logger.error("Couldn't find the labRemote Python libraries")
                logger.info("Make sure to add them to your $PYTHONPATH first by either doing")
                logger.info("    export PYTHONPATH=/path/to/labRemote/build/lib:$PYTHONPATH")
                logger.info("    or writing \"/path/to/labRemote/build/lib\" in \"" + args.equip + "\"")
                logger.info("Also make sure labRemote is built with Python bindings (cmake3 -DUSE_PYTHON=on)")
                exit(1)

    
    if args.upload:
        dbSink = psConfig["influxdb_cfg"]
        pass
    else:
        dbSink = ""
    


    hw = labRemote.ec.EquipConf()
    hw.setHardwareConfig(args.equip)

    doivi(config, args.what, dbSink)





