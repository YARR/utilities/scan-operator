from influxdb import InfluxDBClient, exceptions
import sys, os, time
import json, argparse
from dateutil.parser import parse

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



def getFromInflux(dbSink, query):

    host    = dbSink["host"]
    port    = dbSink["port"]
    usr     = dbSink["username"]
    dbName  = dbSink["database"]
    pwd     = ""

    if "INFLUXDBPWD" in  os.environ:
        # Don't ask the user twice (if the pwd was already entered in a previous stage)
        pwd = str(os.environ['INFLUXDBPWD'])

    try:
        # Try to connect without authentication
        client = InfluxDBClient(host=host,
                                port=port,
                                database=dbName,
                                username=usr,
                                password=pwd)

        # Check if it worked
        database_list=client.get_list_database()
        
    except exceptions.InfluxDBClientError as e:

        # If auth is enabled, ask the user for his/her password
        if "authorization failed" in str(e):
            print("[ warn ] Input the password for user \"{}\" in InfluxDB.\n" 
                  "         If you don\'t want to input the password in the future until\n"
                  "         you close your current bash session use \n"
                  "                  \"export INFLUXDBPWD=yourPassword\".".format(usr))
            try:
                pwd = getpass.getpass()
            except:
                print()
                sys.exit(1)

    # And connect again.
    client=InfluxDBClient(host=host,
                          port=port,
                          database=dbName,
                          username=usr,
                          password=pwd)

    try:
        # This should be working now if the user entered the right pwd
        database_list=client.get_list_database()

    except exceptions.InfluxDBClientError as e:

        # If not, let the user know what's happening and exit.
        print("[ error ][gi] Received error from InfluxDB: {}".format(e))
        print("[ info ][gi] Please specify the db connectivity parameters "
                "\"database\", and also \"username\", " 
                "\"host\" and \"port\" in the .\"influxdb_cfg\" section in" 
                "the provided DCS config file. Make sure the provided username and password are correct.\n")

        sys.exit(1)

    # everything should be ok here

    try:
        data = client.query(query)
    except exceptions.InfluxDBClientError as e:
        print("[ error ][gi] Received error from InfluxDB: {}".format(e))
        sys.exit(1)

    return list(data)




if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-j",
        "--json",
        required=True,
        help="json file where containinfg the db information",
    )

    args = parser.parse_args()

    # - - - - - - - - - -
    with open(args.json) as f:
        config = json.load(f)
    
    with open(args.json) as g:
        dbConfig = json.load(g)

    dbSink = dbConfig["influxdb_cfg"]


    # - - - - - - - - - -
    measName = dbConfig["influxdb_cfg"]["measurement"]
    
    # temperature
    T_columnName = dbConfig["influxdb_cfg"]["temperature"]["column_name"]
    T_sett = dbConfig["influxdb_cfg"]["temperature"]["setting_value"]
    T_hw = dbConfig["influxdb_cfg"]["temperature"]["allowed_half_width"]

    T_query = "select last(" + T_columnName + ") from " + measName
    T_last = getFromInflux(dbSink, T_query)

    if T_last == []:
        print("[ error ][gi] Not found any values of \"{}\" in measurement \"{}\"".format(T_columnName, measName))
        sys.exit(1)

    T_Now = T_last[0][0]["last"]

    # humidity
    H_columnName = dbConfig["influxdb_cfg"]["humidity"]["column_name"]
    H_sett = dbConfig["influxdb_cfg"]["humidity"]["setting_value"]
    H_hw = dbConfig["influxdb_cfg"]["humidity"]["allowed_half_width"]

    H_query = "select last(" + H_columnName + ") from " + measName
    H_last = getFromInflux(dbSink, H_query)

    H_Now = H_last[0][0]["last"]



    dateNow = parse(H_last[0][0]["time"]).date()
    timeNow = parse(H_last[0][0]["time"]).time()


    if abs(T_Now - T_sett) > T_hw or abs(H_Now - H_sett) > H_hw:
        print("Waiting until temperature and humidity reach the operation range...\n")
        
        print("T       H         Time (GMT)         Date\n")

        while abs(T_Now - T_sett) > T_hw or abs(H_Now - H_sett) > H_hw:

            cT = bcolors.OKGREEN if abs(T_Now - T_sett) < T_hw else bcolors.FAIL
            cH = bcolors.OKGREEN if abs(H_Now - H_sett) < H_hw else bcolors.FAIL

            print(cT + "%.2f" % T_Now + cH + "   %.2f " % H_Now + bcolors.ENDC + "    %s    %s" % (timeNow, dateNow))

            T_last = getFromInflux(dbSink, T_query)
            T_Now = T_last[0][0]["last"]

            H_last = getFromInflux(dbSink, H_query)
            H_Now = H_last[0][0]["last"]

            dateNow = parse(H_last[0][0]["time"]).date()
            timeNow = parse(H_last[0][0]["time"]).time()
            
            time.sleep(10)

    else:
        print("[ info ][gf] H and T (%.2f %% and %.2f C) are within the working interval." % (H_Now, T_Now))
        print("[ info ][gf] Proceeding with the scans...")
            


