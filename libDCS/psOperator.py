import argparse
import logging
import pathlib
import sys, os
import signal
import time
import getpass
import json
from influxdb import InfluxDBClient, exceptions
from getFromInfluxDB import getFromInflux


# cfg > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > 
# Maximum difference between module T and DP temperature allowing to safely turn
# the PS off
maxDiffBetweenTandDPtoTurnTheHVoff = 15 # C

# Temperature location in influxDB
temperatureSink = {
        "host": "atlastit01.kek.jp",
        "port": 8086,
        "username": "",
        "database": "dcsDB",
        "measurement": "canary"
    }
tColumnName = "ntc0"

# dew point location in influxDB
dewpointSink = {
        "host": "atlastit01.kek.jp",
        "port": 8086,
        "username": "",
        "database": "dcsDB",
        "measurement": "NewChillerRD53aQuadFlat"
    }
dpColumnName = "Dew Point (ch.Carrier in) [C]"
# < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < 

# e.g. calls

# M the PS until killed (use with screen, etc)
#    python3 libDCS/psOperator.py -e configs/lr_powersupply.json -c LV -d configs/idb_dcs.json -i measure



# LR stuff
LABREMOTE = 1
try:
    import labRemote
except:
    try:
        import _labRemote as labRemote
    except:
        LABREMOTE = 0


# misc
def signal_handler(sig, frame):
    print("done")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)


# ps controlling functions
def set_current(hw, ps, args):
    ps.setCurrentLevel(args.current)
    if args.max_voltage >= 0:
        ps.setVoltageProtect(args.max_voltage)
    return 0

def get_current(hw, ps, args):
    return ps.getCurrentLevel()

def meas_current(hw, ps, args):
    return ps.measureCurrent()

def set_voltage(hw, ps, args):
    ps.setVoltageLevel(args.voltage)
    if args.max_current >= 0:
        ps.setVoltageProtect(args.max_current)
    return 0

def get_voltage(hw, ps, args):
    voltage = ps.getVoltageLevel()
    print(voltage)
    return 0

def meas_voltage(hw, ps, args):
    return ps.measureVoltage()

def power_on(hw, ps, args):
    ps.turnOn()
    return 0

def power_off(hw, ps, args):

    ps.turnOff()
    return 0

#    if (args.force):
#        ps.turnOff()
#        print("[ info ][ps] ps off")     
#        return
#
#
#    t  = getFromInflux(temperatureSink, "select last(" + tColumnName + ") from " + temperatureSink["measurement"])
#    dp = getFromInflux(dewpointSink, "select last(\"" + dpColumnName + "\") from " + dewpointSink["measurement"])
#
#    if t == [] or dp == []:
#        print("[ error ][ps] couldn't find T or DP data in influxDB. The retrieved values are")
#        print("[ info ][ps] T = {}    DP = {}".format(t, dp))
#        print("[ qstn ][ps] turn off the PS anyway?")
#
#        ans = input ("y/n: ")
#        if ans == 'y': 
#            ps.turnOff()
#            print("[ info ][ps] ps off")
#            return
#        else: 
#            print("[ info ][ps] ok!")
#            return
#
#
#
#    t =  t[0][0]["last"]
#    dp = dp[0][0]["last"]
#
#    print("[ info ][ps] T = {:.2f}; dp = {:.2f}".format(t, dp))
#    
#    diff = t - dp
#    if diff < maxDiffBetweenTandDPtoTurnTheHVoff:
#        print("[ warn ][ps] (T - DP) = {:.2f}, which is smaller than {}. Cannot power off the PS in these conditions".format(diff, maxDiffBetweenTandDPtoTurnTheHVoff))
#    else:
#        ps.turnOff()
#        print("[ info ][ps] ps off")
#
#    return

def measure(hw, ps, args):
    # Takes data and pushes it to influxdb

    # Split channels in case more than one is given
    #   e.g. "vdda,vddd" -> ['vdda', 'vddd']
    chList = args.channel.split(',')


    points = [] # a whole row, sharing the same timestamp.

    if not args.dbConnectivity:
        print("-d, please.")
        parser.print_help()
        sys.exit(1)

    with open(args.dbConnectivity) as f:
        psConfig = json.load(f)
    
    dbSink = psConfig["influxdb_cfg"]
    incr = 0 if args.infinity else 1 # incr = 1 -> only one measurement
    a = 0
    while a < 1:
        for ch in chList:
            ps = hw.getPowerSupplyChannel(ch)
            point = {
                "measurement": dbSink["measurement"],
                "fields": {
                    "measV_"+ch: ps.measureVoltage(),
                    "measI_"+ch: ps.measureCurrent(),
                    "settV_"+ch: ps.getVoltageLevel(),
                    "settI_"+ch: ps.getCurrentLevel()
                },
                "tags": {
                    "user": getpass.getuser()
                }
            }
            points.append(point)

        toInfluxDB(points, dbSink)
        time.sleep(2)
        a += incr

# InfluxDB 
def toInfluxDB(points, dbSink, host='localhost', port=8086):

    host    = dbSink["host"]
    port    = dbSink["port"]
    dbName  = dbSink["database"]
    usr     = dbSink["username"]
    pwd     = ""


    if "INFLUXDBPWD" in  os.environ:
        # Don't ask the user twice (if the pwd was already entered in a previous stage)
        pwd = str(os.environ['INFLUXDBPWD'])

    try:    
        # Try to connect without authentication
        client = InfluxDBClient(host=host,
                                port=port,
                                database=dbName,
                                username=usr,
                                password=pwd)

        # Check if it worked
        database_list=client.get_list_database()
        
    except exceptions.InfluxDBClientError as e:

        # If auth is enabled, ask the user for his/her password
        if "authorization failed" in str(e):
            #print("[ warn ] Input the password for user \"{}\" in InfluxDB.\n" 
            #      "         If you don\'t want to input the password in the future until\n"
            #      "         you close your current bash session (e.g. you are running a\n"
            #      "         script that calls this many times), use \n"
            #      "                  \"export INFLUXDBPWD=yourPassword\".".format(usr))
            print("[ warn ] Authorization is enabled in InfluxDB. \n" 
                  "         Please enter the password for user \"{}\".".format(usr))
            sys.exit(2)
            
            try:
                pwd = getpass.getpass()
            except:
                print()
                sys.exit(1)

    # And connect again.
    client=InfluxDBClient(host=host,
                          port=port,
                          database=dbName,
                          username=usr,
                          password=pwd)

    try:
        # This should be working now if the user entered the right pwd
        database_list=client.get_list_database()

    except exceptions.InfluxDBClientError as e:

        # If not, let the user know what's happening and exit.
        print("[ error ][ps] Received error from InfluxDB: {}".format(e))
        print("[ info ][ps] Please specify the db connectivity parameters "
                "\"database\", and also \"username\", " 
                "\"host\" and \"port\" in the .\"influxdb_cfg\" section in" 
                "the provided DCS config file. Make sure the provided username and password are correct.\n")

        sys.exit(1)

    try:
        client.write_points(points, time_precision = 'n')
    except exceptions.InfluxDBClientError as e:
        print("[ error ] Received error from InfluxDB: {}".format(e))
        sys.exit(1)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--channel",
        default="",
        help="Set PS channel (name) to control",
    )
    parser.add_argument(
        "-e",
        "--equip",
        help="Configuration file with the power supply definition",
    )
    parser.add_argument(
        "-d",
        "--dbConnectivity",
        help="Connectivity config for InfluxDB",
    )
    parser.add_argument(
        "-i",
        "--infinity",
        action='store_true',
        help="Measure and Post until the end of the universe (or until killed)",
    )
    #parser.add_argument(
    #    "-f",
    #    "--force",
    #    action='store_true',
    #    help="Force power-off",
    #)
    subparsers = parser.add_subparsers(help="command action")
    parser_set_current = subparsers.add_parser(
        "set-current", help="Set current [A] with maximum voltage [V]"
    )
    parser_set_current.add_argument("current", type=float)
    parser_set_current.add_argument(
        "max_voltage",
        type=float,
        nargs="?",
        default=-1,
        help="Set maximum voltage, -1 for no protection.",
    )
    parser_set_current.set_defaults(func=set_current)

    parser_get_current = subparsers.add_parser(
        "get-current", help="Get set current level [A]"
    )
    parser_get_current.set_defaults(func=get_current)

    parser_meas_current = subparsers.add_parser(
        "meas-current", help="Get reading of current [A]"
    )
    parser_meas_current.set_defaults(func=meas_current)

    parser_set_voltage = subparsers.add_parser(
        "set-voltage", help="Set voltage [V] with maximum current [I]"
    )
    parser_set_voltage.add_argument("voltage", type=float)
    parser_set_voltage.add_argument(
        "max_current",
        type=float,
        nargs="?",
        default=-1,
        help="Set maximum current, -1 for no protection.",
    )
    parser_set_voltage.set_defaults(func=set_voltage)

    parser_get_voltage = subparsers.add_parser(
        "get-voltage", help="Get set voltage level [V]"
    )
    parser_get_voltage.set_defaults(func=get_voltage)

    parser_meas_voltage = subparsers.add_parser(
        "meas-voltage", help="Get reading of voltage [V]"
    )
    parser_meas_voltage.set_defaults(func=meas_voltage)

    parser_power_on = subparsers.add_parser(
        "power-on",
        help="Power ON PS",
    )
    parser_power_on.set_defaults(func=power_on)

    parser_power_off = subparsers.add_parser("power-off", help="Power OFF PS")
    parser_power_off.set_defaults(func=power_off)

    parser_measure = subparsers.add_parser(
        "measure", help="measure I and V and post to influxDB"
    )
    parser_measure.set_defaults(func=measure)



    args = parser.parse_args()

    if not args.equip:
        print("option '-e' is mandatory")
        parser.print_help()
        sys.exit(1)



    if LABREMOTE == 0:

        with open(args.equip) as f:
            lrLibPath = json.load(f)["LR_lib_path"]
        
        sys.path.append(lrLibPath)

        try:
            import labRemote
        except:
            try:
                import _labRemote as labRemote
            except:
                print("Couldn't find the labRemote Python libraries. Make sure to add them to your $PYTHONPATH first by either doing")
                print()
                print("     export PYTHONPATH=/path/to/labRemote/build/lib:$PYTHONPATH")
                print()
                print("  or writing \"/path/to/labRemote/build/lib\" in \"" + args.equip + "\"")
                print()
                print("And make sure also that you have previously built labRemote enabling the Python bindings (cmake3 -DUSE_PYTHON=on)")
                exit(1)


    # - - - - - - - - - -

    hw = labRemote.ec.EquipConf()
    hw.setHardwareConfig(args.equip)
    if not args.channel:
        print("option -c is mandatory")
        parser.print_help()
        sys.exit(1)
        #psReal = hw.getPowerSupply(args.name)
        #ps = labRemote.ps.PowerSupplyChannel(f"{args.name}1", psReal, 1)
    elif ',' in args.channel:
        ps = 0
    else:
        ps = hw.getPowerSupplyChannel(args.channel)



    try:
        tmp = args.func
    except:
        print("You need to specify one command")
        parser.print_help()
        sys.exit(1)



    code = args.func(hw, ps, args)
    sys.exit(code)





