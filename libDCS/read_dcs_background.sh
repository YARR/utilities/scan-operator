#!/bin/bash

chn=""
nChannels="$(jq -j '.channels_to_be_used_by_the_scanoperator | length' $PSCFG)"
for (( i=0; i<$nChannels; i++ ))
do
    chName="$(jq -j ".channels_to_be_used_by_the_scanoperator[$i].name" $PSCFG)"
    chn+="$chName,"
done
chn=${chn::-1}

if [ ! -e $bin ]; then
    echo "[ error ][dm] couldn't find '$bin'"
    exit 1
fi

nMeasurements=0
while [ "$(<$SOBASE/.counters/dcsStat)" == "1" ]
do
    
    python3 $SOBASE/libDCS/psOperator.py -e $1 -c $chn -d $2 measure
    echo "[ info ][dm] DCS measurement performed"
    ((nMeasurements=nMeasurements+1))

done
echo "[ info ][dm] taken a total of $nMeasurements DCS Measurements"
echo "[ info ][dm] finished DCS background data taking"

