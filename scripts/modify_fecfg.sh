#!/bin/bash
#################################
# Contacts: Minoru Hirose
# Email: minoru.hirose at cern.ch
# Date: April 2022
# Project: Scan-Operator
################################

function usage {
    cat <<EOF

This gives a common way to modify FE config files.

    ./scripts/modify_fecfg.sh -m MODSN --feid=1 <-g | -l | -p> [Key1 Value1 Key2 Value2...]

Available options:

    -h | --help         Show this usage
    -m | --module MODSN Module Name (should be defined in configs/so_modules.json)
    -i | --feid CHIP_ID Chip ID for the modification target
    -r | --row          Row address (0-383) for the pixel config modification
    -c | --col          Col address (0-399) for the pixel config modification
    -g | --globalcfg    GlobalConfig to be modified
    -l | --pixelcfg     PixelConfig to be modified
    -p | --parameter    Parameter to be modified
    -v | --verbose      Detailed output

Example:

    ./scripts/modify_fecfg.sh -m MODSN --feid=1 -v -g
    (This command will modify the GlobalConfig values written in the so_modules.json)

    ./scripts/modify_fecfg.sh -m MODSN --feid=1 -v -g SldoAnalogTrim 23
    (This command will modify the GlobalConfig values written in the so_modules.json, and SldoAnalogTrim will be further overwritten as 23)

    ./scripts/modify_fecfg.sh -m MODSN --feid=0 -v -p ADCcalPar "[ -3.14, 0.200 ]" ChipId 1
    ./scripts/modify_fecfg.sh -m MODSN --feid=0 -v -p ADCcalPar [-3.14,0.200] ChipId 1 #same result as above
    (These command will modify the Parameter field with keys "ADCcalPar" and ChipId to the given values.)

Tips:

    You can use various style for the options with an argument as below.
    -m MODSN
    --module MODSN
    --module=MODSN
    All works fine. This is the case of -m, but same applies to other options.

EOF
}

#################################################
# A few colors for echoing
function darkblue(){
    echo "\033[0;34m$1\033[0m"
}
function green(){
    echo "\033[0;32m$1\033[0m"
}
function red(){
    echo "\033[0;31m$1\033[0m"
}
function yellow(){
    echo "\033[0;33m$1\033[0m"
}

function aux_echo(){
    if [ ${verbose} ] || [ ${1} = "error" ]; then
        if [ ${1} = "error" ]; then
            echo -e "$(red [\ error\ ][so])" ${2}
        elif [ ${1} = "warning" ]; then
            echo -e "$(yellow [\ warning\ ][so])" ${2}
        else
            echo "[ info ][so]" ${2}
        fi
    fi
}

##############################################################
# Start of main functionality
##############################################################
# Treatment of arguments
while getopts r:c:gpilhvm:-: opt; do
    optarg="$OPTARG"
    if [[ "$opt" = - ]]; then
        opt="-${OPTARG%%=*}"
        optarg="${OPTARG/${OPTARG%%=*}/}"
        optarg="${optarg#=}"

        if [[ -z "$optarg" ]] && [[ ! "${!OPTIND}" = -* ]]; then
            optarg="${!OPTIND}"
            shift
        fi
    fi

    case "-$opt" in
        -h|--help)
            usage
            exit
            ;;
        -m|--module)
            MODSN=${optarg}
            ;;
        --feid)
            feid=${optarg}
            if [ -z $feid ]; then
                aux_echo "error" "--feid requires a number as an argument (e.g. --feid 1)"
                exit
            fi
            ;;
        -r|--row)
            row=${optarg}
            ;;
        -c|--col)
            col=${optarg}
            ;;
        -g|--globalcfg)
            globalcfg=1
            ;;
        -l|--pixelcfg)
            pixelcfg=1
            ;;
        -p|--parameter)
            parameter=1
            ;;
        -v|--verbose)
            verbose=1
            ;;
        --)
            break
            ;;
        -\?)
            exit 1
            ;;
        --*)
            echo "$0: illegal option -- ${opt##-}" >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

# Sanity check
if [ -z $globalcfg ] && [ -z $pixelcfg ] && [ -z $parameter ]; then
    aux_echo "error" "Either --globalcfg or --pixelcfg or --parameter has to be given."
    exit
elif ([ ! -z $globalcfg ] && [ ! -z $pixelcfg ] && [ -z $parameter ]) || ([ -z $globalcfg ] && [ ! -z $pixelcfg ] && [ ! -z $parameter ]) || ([ ! -z $globalcfg ] && [ -z $pixelcfg ] && [ ! -z $parameter ]) || ([ ! -z $globalcfg ] && [ ! -z $pixelcfg ] && [ ! -z $parameter ]); then
    aux_echo "error" "Only one of --globalcfg or --pixelcfg or --parameter has to be given."
    exit
fi

# Modification part
SCRIPTDIR=$(cd $(dirname $0); pwd)
aux_echo "info" "Module Config Modifier: Module ID = ${MODSN}, ChipID = ${feid}"
CHIPCFG=$(${SCRIPTDIR}/get_parameter.sh -m ${MODSN} --fecfg --feid ${feid})
RD53XL=$(${SCRIPTDIR}/get_parameter.sh -m ${MODSN} -t)
if [[ "$CHIPCFG" == *error* ]]; then
    aux_echo "error" "File not found."
    echo ${CHIPCFG}
    exit
fi
aux_echo "info" "File to be modified: ${CHIPCFG}"
if [ ! -e ${CHIPCFG} ]; then
    aux_echo "error" "File not found: ${CHIPCFG}"
    aux_echo "error" "You need to create a config file first."
    exit
fi

aux_echo "info" "Looking for config updates of ChipID=${feid}..."
#Adding ad-hoc configs modification
while [ "$1" != "" ]
do
  key=$1
  shift
  val=$1
  shift
  if [ "$val" = "" ]; then
      aux_echo "error" "Additional config with key=\"$key\" is provided, but value is not provided."
      exit
  fi
  aux_echo "info" "Ad-hoc config to be modified: ${key} ${val}"
  val=$(echo $val | tr -d " ")
  MOD_CFG="$MOD_CFG ${key} ${val}"
done

#Applying modification for GlobalConfig
if [ ! -z ${globalcfg} ]; then
    aux_echo "info" "Global config to be modified..."
    MOD_CFG="$MOD_CFG $(${SCRIPTDIR}/get_parameter.sh -m ${MODSN} --cfglist --feid ${feid})"
    ary=(`echo $MOD_CFG`)
    for i in `seq 1 ${#ary[@]}`; do
        if [ $((${i} % 2)) = 1 ]; then
            echo ${ary[$i-1]} ${ary[$i]} |
            while IFS=" " read -r -a value; do
                key=${value[0]}
                val=${value[1]}
                currentValue=$(jq -r --arg k ${key} --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)"' ${CHIPCFG})
                if [ "$currentValue" == "null" ] || [ -z $currentValue ]; then
                    if ! [[ "$val" =~ ^[0-9]+$ ]] ; then
                        aux_echo "error" "Skipping (key=$key val=$val)."
                        aux_echo "error" "A value for GrobalConfig should be a number. \"$val\" is given."
                    else
                        jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = $v' ${CHIPCFG} > currentValue && mv currentValue ${CHIPCFG}
                        aux_echo "warning" "Added key \"$key\" with value \"$val\" in the chip config file (GlobalConfig section)"
                    fi
                elif [ "$currentValue" != "$val" ]; then
                    if ! [[ "$val" =~ ^[0-9]+$ ]] ; then
                        jq --arg v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = "\($v)"' ${CHIPCFG} > currentValue && mv currentValue ${CHIPCFG}
                    else
                        jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = $v' ${CHIPCFG} > currentValue && mv currentValue ${CHIPCFG}
                    fi
                    aux_echo "info" "Changed \"$key\" from \"$currentValue\" to \"$val\" in the chip config file (GlobalConfig section)"
                fi
            done
        fi
    done
fi

#Applying modification for PixelConfig
if [ ! -z ${pixelcfg} ]; then
    aux_echo "info" "Pixel config to be modified..."
    if [ -z ${row} ] || [ -z ${col} ]; then
        aux_echo "error" "For pixel confg modification, both --row and --col options have to be given."
        exit
    fi
    if [ ${col} -lt 0 ] || [ ${col} -gt 399 ]; then
        aux_echo "error" "Column address should be within 0-399. ${col} is given."
        exit
    fi
    if [ ${RD53XL} = "RD53A" ]; then
        if [ ${row} -lt 0 ] || [ ${row} -gt 191 ]; then
            aux_echo "error" "Row address should be within 0-191. ${row} is given."
            exit
        fi
    elif [ ${RD53XL} = "RD53B" ]; then
        if [ ${row} -lt 0 ] || [ ${row} -gt 383 ]; then
            aux_echo "error" "Row address should be within 0-383. ${row} is given."
            exit
        fi
    fi
    aux_echo "info" "Pixel address [row,col]=[$row,$col] will be modified."
    key=$(echo $MOD_CFG| cut -d " " -f 1)
    val=$(echo $MOD_CFG| cut -d " " -f 2)
    currentValue=$(jq -r --arg k ${key} --arg t ${RD53XL} --argjson r ${row} --argjson c ${col} '."\($t)".PixelConfig[$c]."\($k)"[$r]' ${CHIPCFG})
    if [ "$currentValue" == "null" ]; then
        aux_echo "error" "Something is wrong. Probably the specified key, $key, is wrong"
        exit
    elif [ "$currentValue" != "$val" ]; then
        jq --argjson v $val --arg k $key --arg t ${RD53XL} --argjson r ${row} --argjson c ${col} '."\($t)".PixelConfig[$c]."\($k)"[$r] = $v' ${CHIPCFG} > currentValue && mv currentValue ${CHIPCFG}
        if [ $? -ne 0 ]; then
            aux_echo "error" "jq return code $?"
        else
            aux_echo "info" "Changed \"$key\" of [row,col]=[$row,$col] from \"$currentValue\" to \"$val\" in the chip config file (PixelConfig section)"
        fi
    else
        aux_echo "info" "Given value is same as the current value. Nothing is modified."
    fi
fi

#Applying modification for Parameters
if [ ! -z ${parameter} ]; then
    aux_echo "info" "FE chip parameters to be modified..."
#    MOD_CFG="$MOD_CFG $(${SCRIPTDIR}/get_parameter.sh -m ${MODSN} --cfglist --feid ${feid})"
    ary=(`echo $MOD_CFG`)
    for i in `seq 1 ${#ary[@]}`; do
        if [ $((${i} % 2)) = 1 ]; then
            echo ${ary[$i-1]} ${ary[$i]} |
            while IFS=" " read -r -a value; do
                key=${value[0]}
                val=${value[1]}
                currentValue=$(jq -r --arg k ${key} --arg r ${RD53XL} '."\($r)".Parameter."\($k)"' ${CHIPCFG})
                if [ "$currentValue" == "null" ]; then
                    jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".Parameter."\($k)" = $v' ${CHIPCFG} > currentValue && mv currentValue ${CHIPCFG}
                    if [ $? -ne 0 ]; then
                        aux_echo "error" "jq return code $?"
                    else
                        aux_echo "warning" "Added key \"$key\" with value \"$val\" in the chip config file (Parameter section)"
                    fi
                elif [ "$currentValue" != "$val" ]; then
                    jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".Parameter."\($k)" = $v' ${CHIPCFG} > currentValue && mv currentValue ${CHIPCFG}
                    if [ $? -ne 0 ]; then
                        aux_echo "error" "jq return code $?"
                    else
                        aux_echo "info" "Changed \"$key\" from \"$currentValue\" to \"$val\" in the chip config file (Parameter section)"
                    fi
                fi
            done
        fi
    done
fi

aux_echo "info" "-- Done."
