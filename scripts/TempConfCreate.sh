#!/bin/sh

#################################################
# A few colors for echoing
function darkblue(){
    echo "\033[0;34m$1\033[0m"
}
function green(){
    echo "\033[0;32m$1\033[0m"
}
function red(){
    echo "\033[0;31m$1\033[0m"
}
function yellow(){
    echo "\033[0;33m$1\033[0m"
}

#Number check
function isNumber(){
    if expr "$1" : "[0-9]*$" >&/dev/null || ( expr "$1" : "[0-9]*\.[0-9]*$" >&/dev/null && [ "$1" != "." ] );then
#        echo "$1 is a number"
        return 1
    else
        echo "$1 is not a number."
        return 0
    fi
}

function areAllNumbers(){
    isNumber $1
    if [ $? -eq 1 ]; then
        return 1
    else
        echo "$(red [\ error\ ][TCC]) $1 is not number!!"
        return 0
    fi
}

#Define some constants(These will be replaced to a script to define constants.)
export SOBASE=$(cd $(dirname $0); cd ../; pwd)
MODULECFG="`$SOBASE/scripts/get_parameter.sh -m my_quad_id --modcfg`"
DEFAULTCON="$SOBASE/configs/default-rd53a-4chip-4x3.json"
#echo "MODULECFG=$MODULECFG"
#MODULECFG="configs/so_modules_test.json"

if [ -z "$1" ]; then
    echo -e "[info][TCC]What is the module name? This script require the name."
    echo -e "Ex: ./scripts/TempConfCreate.sh [your_module_id]"
    exit 1
fi
if test $(grep $1 $MODULECFG | wc -l) -gt 0; then
    echo -e "$(red [\ error\ ][TCC]) The $1 in $MODULECFG is used for the other module. Please change or fix it!"
    exit 1
fi
cat $DEFAULTCON | sed -e "s/my_quad_id/$1/" > tmp.json
#cat tmp.json
jq -s '.[0] * .[1]' tmp.json $MODULECFG > configs/new_modules.json
mv configs/new_modules.json $MODULECFG
rm tmp.json
chipNumber=4
echo "[info][TCC] Do you modify the chip names?"
echo "[info][TCC] Default chip names are"
chipName[0]="hogehoge"
for i in `seq 1 $chipNumber`
do
    chipName[$i]="$1Chip$i"
    echo "[info][TCC] chip$i=${chipName[$i]}"
done


echo -n "[y/n]: "

sleep 1e-5
while read answer; do
    case $answer in
        'y' )
            for i in `seq 1 $chipNumber`
            do
                echo "[info][TCC] What is the name of chip$i?"
                sleep 1e-5
                unset answer2
                while [ -z "${answer2}" ]; do
                    read answer2
                done
                chipName[$i]=$answer2
            done
            echo "[info][TCC] The chip names are"
            for i in `seq 1 $chipNumber`
            do
                echo "[info][TCC] chip$i=${chipName[$i]}"
            done
            echo "[info][TCC] Do you need the futher change of the chip names?"
            echo -n "[y/n]: ";;
        'n' )
            echo "[info][TCC] Changing the chip names."
            cp $MODULECFG tmp.json
            ii=0
            for i in `seq 1 $chipNumber`
            do
                jq --arg a $1 --arg b ${chipName[$i]} --argjson c $ii '. | .modules."\($a)".chips[$c].connectivity.name |= "\($b)"' tmp.json > tmptmp.json
                jq --arg a $1 --arg b ${chipName[$i]} --argjson c $ii '. | .modules."\($a)".chips[$c].Parameter.Name |= "\($b)"' tmptmp.json > tmp.json
                let ii++
                echo "[info][TCC] Changed chip$i to ${chipName[$i]}."
            done
            #less tmp.json
            mv tmp.json $MODULECFG
            break ;;
        *)
            echo "[info][TCC] Unknown option. Please try again!"
            echo "[info][TCC] Do you need the futher change of the chip names?"
            #echo "[info][TCC] Do you modify the chip name?"
            echo -n "[y/n]: "
    esac
done
#less $MODULECFG
