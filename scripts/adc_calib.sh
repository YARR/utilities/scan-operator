#!/bin/bash
#################################
# Contacts: Minoru Hirose
# Email: minoru.hirose at cern.ch
# Date: April 2022
# Project: Scan-Operator
# Usage: ./scripts/adc_calib.sh
################################

original_tty_state=$(stty -g)

function usage {
    cat <<EOF

This script guides you to calibrate ADCs in RD53 chips:

    ./scripts/adc_calib.sh -m MOD_SN

Available options:

    - h         Show this usage
    - m MOD_SN  Module name used in the Scan-Operator

EOF
}

function muxConfigModify(){
    for (( ii=0; ii<$1; ii++ ))
    do
        ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
        if [ ${ENABLED} == "0" ]; then
            continue
        fi
        JSON_FILE=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --fecfg --feid $(( ii+1 )))
        if [ $2 == "before" ]; then
            echo -e "[ info ][adc_calib] Looking for cfg updates in chip $(( ii+1 ))'s configs..."
            MOD_CFG=$(jq -r --argjson a ${ii} --arg b ${MOD_SN} '.modules."\($b)".chips[$a].GlobalConfig | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG)
            #Adding some configs for ADC calibration
            MOD_CFG="$MOD_CFG InjVcalMed 500 InjVcalHigh 3500 MonitorVmonMux 1"
            #Applying modification
            ary=(`echo $MOD_CFG`)
            for i in `seq 1 ${#ary[@]}`; do
                if [ $((${i} % 2)) = 1 ]; then
                    echo ${ary[$i-1]} ${ary[$i]} |
                    while IFS=" " read -r -a value; do
                        key=${value[0]}
                        val=${value[1]}
                        currentValue=$(jq -r --arg k ${key} --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)"' $JSON_FILE)
                        if [ "$currentValue" == "null" ];
                        then
                            jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                            echo -e "$(yellow [\ warn\ ][adc_calib]) Added key \"$key\" with value \"$val\" in the chip config file (GlobalConfig section)"
                        elif [ "$currentValue" != "$val" ];
                        then
                            if ! [[ "$val" =~ ^[0-9]+$ ]] ; then
                                jq --arg v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = "\($v)"' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                            else
                                jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                            fi
                            echo -e "[ info ][adc_calib] Changed \"$key\" from \"$currentValue\" to \"$val\" in the chip config file (GlobalConfig section)"
                        fi
                    done
                fi
            done
        elif [ $2 == "after" ]; then
            echo -e "[ info ][adc_calib] Looking for cfg updates in chip $(( ii+1 ))'s configs..."
            #For "after", only change the MonitorVmonMux 2
            MOD_CFG="MonitorVmonMux 2"
            #Applying modification
            ary=(`echo $MOD_CFG`)
            for i in `seq 1 ${#ary[@]}`; do
                if [ $((${i} % 2)) = 1 ]; then
                    echo ${ary[$i-1]} ${ary[$i]} |
                    while IFS=" " read -r -a value; do
                        key=${value[0]}
                        val=${value[1]}
                        currentValue=$(jq -r --arg k ${key} --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)"' $JSON_FILE)
                        if [ "$currentValue" == "null" ];
                        then
                            jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                            echo -e "$(yellow [\ warn\ ][adc_calib]) Added key \"$key\" with value \"$val\" in the chip config file (GlobalConfig section)"
                        elif [ "$currentValue" != "$val" ];
                        then
                            if ! [[ "$val" =~ ^[0-9]+$ ]] ; then
                                jq --arg v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = "\($v)"' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                            else
                                jq --argjson v $val --arg k $key --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                            fi
                            echo -e "[ info ][adc_calib] Changed \"$key\" from \"$currentValue\" to \"$val\" in the chip config file (GlobalConfig section)"
                        fi
                    done
                fi
            done
        else
            echo "[ error ][adc_calib] Something is wrong!!"
            exit 1
        fi
    done
    echo "[ info ][adc_calib] Successfully modified values..."
}

#Voltage Input/Output
function giveVoltage(){
    if [ $2 == "before" ]; then
        for (( ii=0; ii<$1; ii++ )); do
            ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
            if [ ${ENABLED} == "0" ]; then
                continue
            fi
            read -p "Input Chip $(( ii+1 )) Voltage [mV]: " v_before[${ii}]
        done
    elif [ $2 == "after" ]; then
        for (( ii=0; ii<$1; ii++ )); do
            ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
            if [ ${ENABLED} == "0" ]; then
                continue
            fi
            read -p "Input Chip $(( ii+1 )) Voltage [mV]: " v_after[${ii}]
        done
    else
        echo "[ error ][adc_calib] Something is wrong!!"
        exit 1
    fi
    echo "[ info ][adc_calib] Successfully received values..."
}
function showVoltage(){
    if [ $2 == "before" ]; then
        for (( ii=0; ii<$1; ii++ )); do
            ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
            if [ ${ENABLED} == "0" ]; then
                continue
            fi
            echo "[ info ][adc_calib] Chip $(( ii+1 )) Voltage [mV]: ${v_before[ii]}"
        done
    elif [ $2 == "after" ]; then
        for (( ii=0; ii<$1; ii++ )); do
            ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
            if [ ${ENABLED} == "0" ]; then
                continue
            fi
            echo "[ info ][adc_calib] Chip $(( ii+1 )) Voltage [mV]: ${v_after[ii]}"
        done
    else
        echo "[ error ][adc_calib] Something is wrong!!"
        exit 1
    fi
}
function rd53aVoltageProbe(){
    #################################################
    # Aruguments would be either "before" or "after"
    #################################################
    giveVoltage $1 $2
    showVoltage $1 $2

    #Prompting user inputs about probed voltage measurements
    echo -n "[ info ][adc_calib] Does these values look fine? [y/n]: "
    while read confirmation; do
        case $confirmation in
            'y' )
                areAllNumbers $1 $2
                if [ $? -eq 1 ]; then
                    echo "[ info ][adc_calib] Please try again..."
                    giveVoltage $1 $2
                    showVoltage $1 $2
                    echo -n "[ info ][adc_calib] Does these values look fine? [y/n]: "
                    continue
                fi
                break ;;
            'n' )
                echo "[ info ][adc_calib] Please try again..."
                giveVoltage $1 $2
                showVoltage $1 $2
                echo -n "[ info ][adc_calib] Does these values look fine? [y/n]: "
                continue ;;
            *) echo "unknown option. try again."
                showVoltage $1 $2
                echo -n "[ info ][adc_calib] Does these values look fine? [y/n]: "
        esac
    done
}
function rd53bVoltageProbe(){
    echo "To be implemented..."
}

#Number check
function isNumber(){
    if expr "$1" : "[0-9]*$" >&/dev/null || ( expr "$1" : "[0-9]*\.[0-9]*$" >&/dev/null && [ "$1" != "." ] );then
#        echo "$1 is a number"
        return 0
    else
        echo "[ error ][adc_calib] $1 is not a number."
        return 1
    fi
}
function areAllNumbers(){
    for (( ii=0; ii<$1; ii++ )); do
        ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
        if [ ${ENABLED} == "0" ]; then
            continue
        fi
        if [ $2 == "before" ]; then
            isNumber ${v_before[ii]}
        elif [ $2 == "after" ]; then
            isNumber ${v_after[ii]}
        else
            echo "[ error ][adc_calib] Something is wrong!!"
            exit 1
        fi
        if [ $? -eq 1 ]; then
            return 1
        fi
    done
    return 0
}

#ADC Calibratied Parameters
function rd53aAdcReading(){
    for (( ii=0; ii<$1; ii++ ))
    do
        ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
        if [ ${ENABLED} == "0" ]; then
            continue
        fi
        chipName="$(jq -j --argjson a ${ii} --arg b ${MOD_SN} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)"
        #Scan list has to be modified!!
        ADCREAD=$(${SO} -m $MOD_SN -l reg_read | grep -e "MON MUX_V: 1," -e "MON MUX_V: 2," | grep $chipName)
        if [ $? -eq 1 ]; then
            echo "[ error ][adc_calib] Something wrong in the command below..."
            echo ${SO} -m $MOD_SN -l reg_read
        fi
        if [ $2 == "before" ]; then
            adc_before[${ii}]=$(echo $ADCREAD| cut -d " " -f 9)
            isNumber ${adc_before[${ii}]}
            if [ $? -eq 1 ]; then
                echo "[ error ][adc_calib] Couldn't read ADC."
                exit 1
            fi
        elif [ $2 == "after" ]; then
            adc_after[${ii}]=$(echo $ADCREAD| cut -d " " -f 21)
            isNumber ${adc_after[${ii}]}
            if [ $? -eq 1 ]; then
                echo "[ error ][adc_calib] Couldn't read ADC."
                exit 1
            fi
        else
            echo "[ error ][adc_calib] Something is wrong!!"
            exit 1
        fi
        echo "[ info ][adc_calib] Successfully received values..."
    done
    showAdc $1 $2
}
function showAdc(){
    if [ $2 == "before" ]; then
        for (( ii=0; ii<$1; ii++ )); do
            ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
            if [ ${ENABLED} == "0" ]; then
                continue
            fi
            echo "[ info ][adc_calib] Chip $(( ii+1 )) ADC: ${adc_before[ii]}"
        done
    elif [ $2 == "after" ]; then
        for (( ii=0; ii<$1; ii++ )); do
            ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
            if [ ${ENABLED} == "0" ]; then
                continue
            fi
            echo "[ info ][adc_calib] Chip $(( ii+1 )) ADC: ${adc_after[ii]}"
        done
    else
        echo "[ error ][adc_calib] Something is wrong!!"
        exit 1
    fi
}

#Slope and ofset computation
function deriveLinearFunction(){
    for (( ii=0; ii<$1; ii++ )); do
        ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
        if [ ${ENABLED} == "0" ]; then
            continue
        fi
        JSON_FILE=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --fecfg --feid $(( ii+1 )))
        slope[${ii}]=`echo "scale=5; (${v_after[${ii}]}-${v_before[${ii}]}) / (${adc_after[${ii}]}-${adc_before[${ii}]})" | bc`
        offset[${ii}]=`echo "scale=5; ${v_before[${ii}]} - ${slope[${ii}]} * ${adc_before[${ii}]}" | bc`
        echo "[ info ][adc_calib] Chip $(( ii+1 )): (offset,slope) = (${offset[${ii}]},${slope[${ii}]})"
    done
}
function writeLinearFunction(){
    for (( ii=0; ii<$1; ii++ )); do
        ENABLED=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --enabled --feid $(( ii+1 )))
        if [ ${ENABLED} == "0" ]; then
            continue
        fi
        JSON_FILE=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} --fecfg --feid $(( ii+1 )))
        MOD_CFG="ADCcalPar ${offset[${ii}]} ${slope[${ii}]}"
        #Applying modification
        ary=(`echo $MOD_CFG`)
        for i in `seq 1 ${#ary[@]}`; do
            if [ $((${i} % 3)) = 1 ]; then
                echo ${ary[$i-1]} ${ary[$i]} ${ary[$i+1]}|
                while IFS=" " read -r -a value; do
                    key=${value[0]}
                    ofs=${value[1]}
                    slp=${value[2]}
                    currentValue=$(jq -r --arg k ${key} --arg r ${RD53XL} '."\($r)".Parameter."\($k)"' $JSON_FILE)
                    currentValue=`echo ${currentValue} | sed -e "s/[\r\n]\+//g"`
                    if [ "$currentValue" == "null" ];
                    then
                        jq --argjson a $ofs --arg k $key --arg r ${RD53XL} '."\($r)".Parameter."\($k)"[0] = $a' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                        jq --argjson a $slp --arg k $key --arg r ${RD53XL} '."\($r)".Parameter."\($k)"[1] = $a' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                        echo -e "[\ warn\ ][adc_calib] Added key \"$key\" with value \"[$ofs,$slp]\" in the chip config file (Parameter section)"
                    elif [ "$currentValue" != "$val" ];
                    then
                        jq --argjson a $ofs --arg k $key --arg r ${RD53XL} '."\($r)".Parameter."\($k)"[0] = $a' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                        jq --argjson a $slp --arg k $key --arg r ${RD53XL} '."\($r)".Parameter."\($k)"[1] = $a' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                        echo -e "[ info ][adc_calib] Changed \"$key\" from \"$currentValue\" to \"[$ofs,$slp]\" in the chip config file (Parameter section)"
                    fi
                done
            fi
        done
    done
}
#Safe for SIGINT
function wrapAndExit(){
    echo "[ info ][adc_calib] Wrapping and exiting after getting an error"
    echo "[ info ][adc_calib] Reverting the terminal to its original state..."
    stty $original_tty_state
    exit 1
}

function sigintStop(){
    echo -e "\n[ info ][adc_calib] Stoped by SIGINT"
    wrapAndExit
}

trap 'sigintStop' 2


##############################################################
# Start of main functionality
##############################################################
# Treatment of arguments
while getopts m:h OPT
do
    case ${OPT} in
        m ) MOD_SN=${OPTARG} ;;
        h ) usage
            exit ;;
        * ) usage
            exit ;;
    esac
done

# Exit if no module is given
[ -z ${MOD_SN} ] && usage && exit

# Existence check of the Scan-Operator
DIRBASE=$(cd $(dirname $0); pwd)
SO=$DIRBASE/../ScanOperator.sh
if [ ! -e $SO ]; then
    echo "[ info ][adc_calib] ScanOperator.sh not found ($SO)";
    exit 1
fi
# Preparing config files which are common with Scan-Operator
INDEXJSON=$DIRBASE/../configs/so_index.json
if [ ! -f $INDEXJSON ]; then
    echo -e "$(red [\ error\ ][adc_calib]) Not found file \"$INDEXJSON\""
    echo -e "$(red [\ error\ ][adc_calib]) Put the file back there and try again."
    exit 1
fi
MODULECFG="$DIRBASE/../`jq -j '.module_config' $INDEXJSON`"

#Module existence check
value="$(cat $MODULECFG | jq --arg a $MOD_SN '.modules."\($a)"')"
if [ "$value" = "null" ]; then
    echo "[ error ][adc_calib] No module ID \"$MOD_SN\" found under the entry \"modules\" in $MODULECFG."
    echo "[ info  ][adc_calib] Please add yours (or rename one of the above) and try again."
    exit 1
fi

# Retrieve the number of chips to calibrate
SCRIPTDIR=$(cd $(dirname $0); pwd)
NCHIPS=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} -n)
RD53XL=$(${SCRIPTDIR}/get_parameter.sh -m ${MOD_SN} -t)

#######################################################
# Preparing the first config
echo "[ info ][adc_calib] Saving the original config..."
echo "[ info ][adc_calib] Performing the config modification..."
muxConfigModify $NCHIPS before
# Sending the first config
echo "[ info ][adc_calib] Configuring the frontend chips..."
${SO} -m $MOD_SN -A > /dev/null
echo "[ info ][adc_calib] Waiting for the voltage probe results..."

rd53aVoltageProbe $NCHIPS before

echo "[ info ][adc_calib] Waiting for the ADC reading..."
rd53aAdcReading $NCHIPS before

#######################################################
# Preparing the second config
echo "[ info ][adc_calib] Performing the config modification..."
muxConfigModify $NCHIPS after
# Sending the second config
echo "[ info ][adc_calib] Configuring the frontend chips..."
${SO} -m $MOD_SN -A > /dev/null
echo "[ info ][adc_calib] Waiting for the voltage probe results..."

rd53aVoltageProbe $NCHIPS after

echo "[ info ][adc_calib] Waiting for the ADC reading..."
rd53aAdcReading $NCHIPS after

#######################################################
# Computing the slopes and ofsets
echo "[ info ][adc_calib] Computing the calibrated ADC slopes and offsets..."
deriveLinearFunction $NCHIPS
echo "[ info ][adc_calib] Writing these values in the config file..."
writeLinearFunction $NCHIPS
echo "-- Done."


#TODO
#When Ctrl+C is hit, restore the original config...
