#!/bin/sh

function usage {
    cat <<EOF

This gives a common way to parse and retrieve parameters in various config files.

    ./scripts/adc_sourcescan_rd53a.sh -c INDEXFILE -t ScanTime -f Frequency

Available options:

    -h | --help                  Show this usage
    -c | --config INDEXJSON      ScanOperator index file (default: configs/so_index.json)
    -t | --time ScanTime         How long do you want to execute source scan?(In second.)
    -f | --frequency Frequency   How many frequency do you use in your sourcescan?

EOF
}

#################################################
# A few colors for echoing
function darkblue(){
    echo "\033[0;34m$1\033[0m"
}
function green(){
    echo "\033[0;32m$1\033[0m"
}
function red(){
    echo "\033[0;31m$1\033[0m"
}
function yellow(){
    echo "\033[0;33m$1\033[0m"
}
function aux_echo(){
    #if [ ${verbose} ] || [ ${1} = "error" ]; then
    if [ ${1} = "error" ]; then
        echo -e "$(red [\ error\ ][so])" ${2}
    elif [ ${1} = "warning" ]; then
        echo -e "$(yellow [\ warning\ ][so])" ${2}
    else
        echo "[ info ][so]" ${2}
    fi
    #fi
}

##############################################################
# Start of main functionality
##############################################################
# Treatment of arguments
ChangeTime=0
ChangeFreq=0

while getopts hc:t:f: opt; do
    optarg="$OPTARG"
    if [[ "$opt" = - ]]; then
        opt="-${OPTARG%%=*}"
        optarg="${OPTARG/${OPTARG%%=*}/}"
        optarg="${optarg#=}"

        if [[ -z "$optarg" ]] && [[ ! "${!OPTIND}" = -* ]]; then
            optarg="${!OPTIND}"
            shift
        fi
    fi

    case "-$opt" in
        -h|--help)
            usage
            exit
            ;;
        -c|--config)
            INDEXJSON=${optarg}
            ;;
        -t|--time)
            SCANTIME=${optarg}
            ChangeTime=1
            ;;
        -f|--frequency)
            Freq=${optarg}
            ChangeFreq=1
            ;;
        *)
            echo "$0: illegal option -- ${opt##-}" >&2
            exit 1
            ;;
    esac
done

# Setup the most important config file.
SCRIPTDIR=$(cd $(dirname $0); pwd)
SOBASE=$(dirname $SCRIPTDIR)
if [ -z ${INDEXJSON} ]; then
    INDEXJSON=$SOBASE/configs/so_index.json
fi
if [ ! -e $INDEXJSON ]; then
    aux_echo "error" "file not found ($INDEXJSON)"
    exit 1
fi

aux_echo "info" "This is a script for generating source scan configs of rd53a in YARR."
aux_echo "info" "Do you want to generate them?"
echo -n "[y/n]: "

sleep 1e-5
while read answer; do
    case $answer in
        'y' )
            aux_echo "info" "OK. Generating them..."
            SCANCFG=$"$SOBASE/`jq -j '.rd53a_scans_config' $INDEXJSON`"
            YARRBASE=`jq -j '.YARR_directory' $SCANCFG`
            ls -1 $YARRBASE/configs/scans/rd53a/*noisescan* > noisetmp
            while read line
            do
                AfModLine=$(echo $line | sed -e 's/noisescan/sourcescan/g')
                #echo $AfModLine
                cp $line $AfModLine
                if [ $ChangeTime = 1 ]; then
                    ii=0
                    while [ 1 ]
                    do
                        Action=$(jq --argjson a $ii '.scan.loops[$a].loopAction' $line)
                        if [ "$Action" == null ]; then
                            aux_echo "error" "Coludn't find Rd53aTriggerLoop in $line! This mean the file doesn't have a key to decide scan time!"
                            break
                        fi
                        #if [ $Action = "Rd53aTriggerLoop" ]; then
                        if echo $Action | grep "Rd53aTriggerLoop" >>/dev/null; then
                            #echo -e "Changed!"
                            jq --argjson a $SCANTIME --argjson b $ii '. | .scan.loops[$b].config.time |= $a' $AfModLine > tmp.json
                            mv tmp.json $AfModLine
                            break
                        fi
                        let ii++
                    done
                fi
                if [ $ChangeFreq = 1 ]; then
                    ii=0
                    while [ 1 ]
                    do
                        Action=$(jq --argjson a $ii '.scan.loops[$a].loopAction' $line)
                        if [ "$Action" == null ]; then
                            aux_echo "error" "Coludn't find Rd53aTriggerLoop in $line! This mean the file doesn't have a key to decide scan frequency!"
                            break
                        fi
                        #if [ $Action = "Rd53aTriggerLoop" ]; then
                        if echo $Action | grep "Rd53aTriggerLoop" >>/dev/null; then
                            #echo -e "Changed!"
                            jq --argjson a $Freq --argjson b $ii '. | .scan.loops[$b].config.frequency |= $a' $AfModLine > tmp.json
                            mv tmp.json $AfModLine
                            break
                        fi
                        let ii++
                    done
                fi
            done<noisetmp
            #cat noisetmp
            rm noisetmp
            break;;
        'n' )
            aux_echo "info" "OK. Thank you."
            exit
            ;;
        *)
            aux_echo "info" " Unknown option. Please try again!"
            aux_echo "info" "Do you want to generate source scan configs?"
            echo -n "[y/n]: "
    esac
done
