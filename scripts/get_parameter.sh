#!/bin/bash
#################################
# Contacts: Minoru Hirose
# Email: minoru.hirose at cern.ch
# Date: April 2022
# Project: Scan-Operator
################################

function usage {
    cat <<EOF

This gives a common way to parse and retrieve parameters in various config files.

    ./scripts/get_parameter.sh -m MODSN [--fetype, --???]

Available options:

    -h | --help              Show this usage
    -c | --config INDEXJSON  ScanOperator index file (default: configs/so_index.json)
    -m | --module MODSN      Module Name (should be defined in configs/so_modules.json)
    -t | --fetype            Returns FETYPE (RD53A or RD53B)
    -n | --nchips            Returns the number of chips
    --modcfg                 Returns a path to the module config file (normally configs/so_modules.json)
    --fecfg                  Retrieves a path to the FE chip config. ChipId has to be given by --feid.
    --feid ChipID            Specifies ChipID for the target FE chip.
    --cfglist                Retrieves a list of module configs written in the moduel config file (i.e. so_omdules.json).
    --enabled                Check if the given feid is enabled or not
    -v | --verbose           Verbose mode

Examples:

    ./scripts/get_parameter.sh -m my_quad_id -t
    (This command returns RD53A on stdout)

    ./scripts/get_parameter.sh --feid=0 --fecfg
    (This command retuns a list of config values which is to be altered from the YARR default values)

EOF
}

#################################################
# A few colors for echoing
function darkblue(){
    echo "\033[0;34m$1\033[0m"
}
function green(){
    echo "\033[0;32m$1\033[0m"
}
function red(){
    echo "\033[0;31m$1\033[0m"
}
function yellow(){
    echo "\033[0;33m$1\033[0m"
}

function aux_echo(){
    if [ ${verbose} ] || [ ${1} = "error" ]; then
        if [ ${1} = "error" ]; then
            echo -e "$(red [\ error\ ][so])" ${2}
        elif [ ${1} = "warning" ]; then
            echo -e "$(yellow [\ warning\ ][so])" ${2}
        else
            echo "[ info ][so]" ${2}
        fi
    fi
}

##############################################################
# Getter
##############################################################
function get_fetype(){
    RD53XL=$(jq -j --arg a $MODSN '.modules."\($a)".chipType' $MODULECFG)
    RD53XS=${RD53XL,,}
}

function get_nchip(){
    nChips=$(jq -j --arg b $MODSN '.modules."\($b)".chips | length' $MODULECFG)
}

function get_index4chipid(){
    #This tries to find an index of the chip list for a given ChipID
    local index found
    found=0
    for (( ii=0; ii<$nChips; ii++ )); do
        id=$(jq -j --argjson a $ii --arg b ${MODSN} '.modules."\($b)".chips[$a].Parameter.ChipId' $MODULECFG)
        if [ $id = $1 ]; then
            index=$ii
            if [ $found -eq 1 ]; then
                aux_echo "error" "Given ChipID=$1 is found twice for a module \"$MODSN\". Please check and correct it."
                exit 1
            fi
            found=1
        fi
    done
    if [ -z $index ]; then
        aux_echo "error" "Given ChipID=$1 is not found for a module \"$MODSN\"."
        exit 1
    fi
    return $index
}

##############################################################
# Start of main functionality
##############################################################
# Treatment of arguments
while getopts nhvtc:m:-: opt; do
    optarg="$OPTARG"
    if [[ "$opt" = - ]]; then
        opt="-${OPTARG%%=*}"
        optarg="${OPTARG/${OPTARG%%=*}/}"
        optarg="${optarg#=}"

        if [[ -z "$optarg" ]] && [[ ! "${!OPTIND}" = -* ]]; then
            optarg="${!OPTIND}"
            shift
        fi
    fi

    case "-$opt" in
        -h|--help)
            usage
            exit
            ;;
        -m|--module)
            MODSN=${optarg}
            ;;
        -c|--config)
            INDEXJSON=${optarg}
            ;;
        -t|--fetype)
            fetype=1
            ;;
        -n|--nchips)
            nchips=1
            ;;
        --modcfg)
            modcfg=1
            ;;
        --fecfg)
            fecfg=1
            ;;
        --feid)
            feid=${optarg}
            if [ -z $feid ]; then
                aux_echo "error" "--feid requires a number as an argument (e.g. --feid 1)"
                exit
            fi
            ;;
        --cfglist)
            cfglist=1
            ;;
        --enabled)
            enabled=1
            ;;
        -v|--verbose)
            verbose=1
            ;;
        --)
            break
            ;;
        -\?)
            exit 1
            ;;
        --*)
            echo "$0: illegal option -- ${opt##-}" >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

# Setup the most inportant config file.
SCRIPTDIR=$(cd $(dirname $0); pwd)
SOBASE=$(dirname $SCRIPTDIR)
if [ -z ${INDEXJSON} ]; then
    INDEXJSON=$SOBASE/configs/so_index.json
fi
if [ ! -e $INDEXJSON ]; then
    aux_echo "error" "file not found ($INDEXJSON)"
    exit 1
fi
aux_echo "info" "Retrieving parameters based on $INDEXJSON"

# Exit if no module is given
aux_echo "info" "Module Name: ${MODSN}"
if [ -z ${MODSN} ];then
    aux_echo "error" "Please specify a Module ID (option '-m'). "
    usage
    exit 1
fi

MODULECFG="$SOBASE/`jq -j '.module_config' $INDEXJSON`"
aux_echo "info" "Module Config: ${MODULECFG}"
if [ ! -z $modcfg ]; then
    echo ${MODULECFG}
    exit 1
fi

#Check whether the module exists in $MODULECFG.
value="$(cat $MODULECFG | jq --arg a $MODSN '.modules."\($a)"')"
if [ "$value" = "null" ]; then
    aux_echo "error" "No module ID \"$MODSN\" found under the entry \"modules\" in $MODULECFG."
    aux_echo "info" "Only the following module IDs are currently defined in the config file:"
    jq -r '.modules | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG |
    while IFS=" " read -r -a value; do
        key=${value[0]}
        aux_echo "info" "-   $key"
    done
    aux_echo "info" "Please add yours (or rename one of the above) and try again."
    exit 1
fi

##############################################
# Preparation of various parameter retrieval
##############################################
get_fetype #Define $RD53XL and RD53XS
get_nchip #Define #nChips
aux_echo "info" "FE TYPE: $RD53XL"
aux_echo "info" "#FEs: $nChips"

#Show FE Type
if [ ! -z ${fetype} ]; then
    echo $RD53XL
fi

#Show #Chips
if [ ! -z ${nchips} ]; then
    echo $nChips
fi

#Sanity checks
if [ ! -z ${feid} ]; then
    if [ -z ${fecfg} ] && [ -z ${cfglist} ] && [ -z ${enabled} ]; then
        aux_echo "error" "Either --fecfg or --cfglist has to be given when you use --feid."
        exit 1
    fi
fi
if [ ! -z ${fecfg} ] || [ ! -z ${cfglist} ]; then
    if [ -z ${feid} ]; then
        aux_echo "error" "ChipID has to be given by --feid=XXX."
        exit 1
    fi
fi
if [ ! -z ${fecfg} ] || [ ! -z ${enabled} ]; then
    if [ -z ${feid} ]; then
        aux_echo "error" "ChipID has to be given by --feid=XXX."
        exit 1
    fi
fi

#Operation for a give ChipID
if [ ! -z ${feid} ]; then
    #Show FE Config Path
    if [ ! -z ${fecfg} ]; then
        aux_echo "info" "FE config path retrieval function is enabled."
        cfgdir=$SOBASE/configs/$RD53XS/$MODSN
        get_index4chipid $feid
        chipIndex=$?
        chipName=$(jq -j --argjson a $chipIndex --arg b ${MODSN} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)
        FECFG=$cfgdir/$chipName.json
        aux_echo "info" "Config directory for ${MODSN}: $cfgdir"
        aux_echo "info" "Chip Name of ID=${feid}: $chipName"
        aux_echo "info" "Config File for ${chipName}: $FECFG"
        if [ ! -e $FECFG ]; then
            aux_echo "warning" "Config File is not yet created."
        fi
        echo $FECFG
    fi
    #Show FE Config Parameter List
    if [ ! -z ${cfglist} ]; then
        aux_echo "info" "FE config parameter retrieval function is enabled."
        cfgdir=$SOBASE/configs/$RD53XS/$MODSN
        get_index4chipid $feid
        chipIndex=$?
        chipName=$(jq -j --argjson a $chipIndex --arg b ${MODSN} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)
        FECFG=$cfgdir/$chipName.json
        MOD_CFG=$(jq -r --argjson a $chipIndex --arg b ${MODSN} '.modules."\($b)".chips[$a].GlobalConfig | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG)
        echo $MOD_CFG
    fi
    #Show if FE chip is enabled or not
    if [ ! -z ${enabled} ]; then
        aux_echo "info" "FE enabled checker is enabled."
        get_index4chipid $feid
        chipIndex=$?
        enable_flag=$(jq -j --argjson a $chipIndex --arg b ${MODSN} '.modules."\($b)".chips[$a].connectivity.enable' $MODULECFG)
        echo $enable_flag
    fi
fi
