#!/bin/sh

original_tty_state=$(stty -g)
dcsOnOff=0      # Turns the DCS on (off) before (after) running all the scans
quietMode=0     # Trims the output to the DB-related info + the scan results
createConfig=0  # Creates / overwrites Yarr config files based on config.json
justConfigure=0 # Update config and exit
skipCfgCheck=0  # Skips checking for cfg updates (saving time)
dcsMonitor=0    #
envMonitor=0    #
PDOpt=0         # Downloads chip config files from Production DB
scan_list="scan_list" #Define scan_list as the default scan list name
ApplyConfig=0
TempInfo=0      # Add temperature info to scan by -T option of YARR.()
resetConfig=0   # Reset Yarr chip config

# Two important constants here
export SOBASE=$(cd $(dirname $0); pwd)
INDEXJSON="$SOBASE/configs/so_index.json"

#################################################
# A few colors for echoing
function darkblue(){
    echo "\033[0;34m$1\033[0m"
}
function green(){
    echo "\033[0;32m$1\033[0m"
}
function red(){
    echo "\033[0;31m$1\033[0m"
}
function yellow(){
    echo "\033[0;33m$1\033[0m"
}

function wrapAndExit(){
    echo "[ info ][so] Wrapping and exiting after getting an error"

    echo -e "\n-------------------------"

    echo "[ info ][so] Recording total elapsed time..."
    elp=$(echo "$(date +%s.%N) - $START_SO" | bc)
    echo -ne "total_so     $elp\n" >> $TIMEFILE


    echo "[ info ][so] Reverting the terminal to its original state..."
    stty $original_tty_state

    if [[ $dcsMonitor == 1 ]]; then
        echo "[ info ][so] waiting for the dcs monitor script to finish..."
        echo "0" > $SOBASE/.counters/dcsStat
        waitForItToFinish "read_dcs_backgr"
    fi

    echo "[ info ][so] -- done -- "
    exit 1
}

function sigintStop(){
    echo -e "\n[ info ][so] Stoped by SIGINT"
    wrapAndExit
}

trap 'sigintStop' 2

function waitForItToFinish(){
    while [ "$(pgrep -x "$1")" != "" ]
    do
        sleep 0.1
    done
}

function usage(){
    cat <<EOF
$0
Help:
  -m [str]   : Serial number of the module
  -l [str]   : The name of scan list (default = "scan_list")
  -W         : upload Yarr's results to localDB (scanConsole's -W)
  -o         : turn on / off the output of the PS before and after all the scans
  -e         : Check if T and H are within a setting range before running each scan
  -d         : Monitor the PS and upload the DCS data to influxDB.
  -t         : Move DCS data from InfluxDB to LocalDB after each scan.
  -c         : Create / override Yarr's config files for your chip.
  -s         : Don't look for updates in the config (saves time)
  -j         : Just update the configs, and exit before running any scan
  -q         : "quiet" mode (cleaner output).
  -P         : Download config files of ITk Pixel Modules from the ITkPD.(Not yet implemented.)
  -A         : Only apply the chip config on target module. Not execute scan.
  -R         : Reset Yarr chip config.(Not yet implemented.)
EOF
    exit 1
}

# Check now if jq exists to prevent further weird-looking jq-related errors
if ! command -v jq &> /dev/null
then
    echo -e "$(red [\ error\ ][so]) command \"jq\" not found! Please install it before proceeding."
    exit
fi

# Check now if unbuffer exists to prevent further weird-looking unbuffer-related errors
if ! command -v unbuffer &> /dev/null
then
    echo -e "$(red [\ error\ ][so]) command \"unbuffer\" not found! That is included in expect. Please install expect before proceeding."
    exit
fi

while getopts m:q:l:docstejWQPAR OPT
do
    case $OPT in
        m) module_id=$OPTARG ;;
        q) quietMode=1 ;;
        l) scan_list=$OPTARG ;;
        t) syncOption="-t" ;;
        e) envMonitor=1 ;;
        o) dcsOnOff=1 ;;
        d) dcsMonitor=1 ;;
        c) createConfig=1 ;;
        s) skipCfgCheck=1 ;;
        j) justConfigure=1 ;;
        W) localdbOpt="-W" ;;
        P) PDOpt=1 ;;
        Q) qcOpt="-Q" ;;
        A) ApplyConfig=1 ;;
        R) resetConfig=1;;
        *) usage ;;
    esac
done

echo "--------------------------------"

if [ ! -f $INDEXJSON ];then
    echo -e "$(red [\ error\ ][so]) Not found file \"$INDEXJSON\""
    echo -e "$(red [\ error\ ][so]) Put the file back there and try again."
    usage
    exit
fi

if [[ $syncOption == "-t" ]] && [[ $localdbOpt == "" ]]; then
    echo -e "$(red [\ error\ ][so]) To transfer DCS data to localDB, first you have to have"
    echo "              the module information in localDB! Option \"-t\" requires also \"-W\"."
    exit
fi


# some value
mkdir -p $SOBASE/.counters
touch $SOBASE/.counters/runNumber $SOBASE/.counters/today

if ! [[ "$(<$SOBASE/.counters/runNumber)" =~ ^[0-9]+$ ]]; then
    echo "0" > $SOBASE/.counters/runNumber
fi
if [[ $(date "+%y%m%d") -gt $(<$SOBASE/.counters/today) ]]; then
    echo $(date "+%y%m%d") > $SOBASE/.counters/today
    echo "0" > $SOBASE/.counters/runNumber
fi

echo "$(($(<$SOBASE/.counters/runNumber)+1))" > $SOBASE/.counters/runNumber

export SO_RUNNUMBER="$(<$SOBASE/.counters/today)_$(printf "%03d" $(<$SOBASE/.counters/runNumber))"

# data folder
mkdir -p $SOBASE/data/by_runnumber/$SO_RUNNUMBER
mkdir -p $SOBASE/data/by_moduleid/$module_id
ln -s $SOBASE/data/by_runnumber/$SO_RUNNUMBER $SOBASE/data/by_moduleid/$module_id/

# time data
touch $SOBASE/data/by_runnumber/$SO_RUNNUMBER/time.dat
export TIMEFILE="$SOBASE/data/by_runnumber/$SO_RUNNUMBER/time.dat"
echo -ne "# what     Elapsed (s)\n" >> $TIMEFILE

# start measuring time
START_SO=$(date +%s.%N)

# Config files used here and there
MODULECFG="$SOBASE/`jq -j '.module_config' $INDEXJSON`"
INFLUXDBENVCFG="$SOBASE/`jq -j '.influxdb_config_env' $INDEXJSON`"

#Start scan
{
#Check whether the module exists in $MODULECFG.
value="$(cat $MODULECFG | jq --arg a $module_id '.modules."\($a)"')"
if [ "$value" = "null" ]; then
    echo -e "$(red [\ error\ ][so]) No module ID \"$module_id\" found under the entry \"modules\" in $MODULECFG."
    echo "[ info  ][so] Only the following module IDs are currently defined in the config file:"

    jq -r '.modules | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG |
    while IFS=" " read -r -a value; do
        key=${value[0]}
        echo "-   $key"
    done

    echo "[ info  ][so] Please add yours (or rename one of the above) and try again."
    exit
fi
#Check RD53A or RD53B
export RD53XL=$(jq -j --arg a $module_id '.modules."\($a)".chipType' $MODULECFG)
export RD53XS=${RD53XL,,}
ScanFlow=${RD53XS}_scans_config
export INFLUXDBDCSCFG="$SOBASE/`jq -j '.influxdb_config_dcs' $INDEXJSON`"
export SCANCFG="$SOBASE/$(jq --arg a $ScanFlow -j '."\($a)"' $INDEXJSON)"
export DBCOMCFG="$SOBASE/`jq -j '.dbs_communication_config' $INDEXJSON`"
export PSCFG="$SOBASE/`jq -j '.powersupply_config' $INDEXJSON`"
export LDBUSRCFG="$SOBASE/`jq -j '.localdb_user_config' $INDEXJSON`"
export LDBSITCFG="$SOBASE/`jq -j '.localdb_site_config' $INDEXJSON`"

export YARRBASE=`jq -j '.YARR_directory' $SCANCFG`

#Check whether the module exists in $SCANCFG.
value="$(cat $SCANCFG | jq --arg a $scan_list '."\($a)"')"
if [ "$value" = "null" ]; then
    echo -e "$(red [\ error\ ][so]) No scan sequence \"$scan_list\" found in $SCANCFG."
    echo "[ info  ][so] Only the following scan sequences are currently defined in the config file:"

    jq -r '. | keys[] as $k | "\($k) \(.[$k])"' $SCANCFG |
    while IFS=" " read -r -a value; do
        key=${value[0]}
        if echo "${key}" | grep -v "YARR_directory\|common_config" >>/dev/null; then
            echo "-   $key"
        fi
    done

    echo "[ info  ][so] Please add yours (or rename one of the above) and try again."
    exit
fi

if [ -z $module_id ];then
    echo "Please specify a Module ID (option '-m'). "
    usage
fi

if [ ! -d "$YARRBASE" ]; then
    echo -e "$(red [\ error\ ][so]) Please specify a valid path to Yarr under \n" \
            "             \".YARR_directory\" in \"$SCANCFG\""
    exit 1
fi


if [[ $dcsOnOff ]] || [[ $dcsMonitor ]]
then
    lrLibPath=`jq -j '.LR_lib_path' $PSCFG`
    export PYTHONPATH=$lrLibPath:$PYTHONPATH
fi


if  [[ $dcsMonitor == 1 ]]
then
    chn=""

    nChannels="$(jq -j '.channels_to_be_used_by_the_scanoperator | length' $PSCFG)"
    for (( i=0; i<$nChannels; i++ ))
    do
        chName="$(jq -j ".channels_to_be_used_by_the_scanoperator[$i].name" $PSCFG)"
        chn+="$chName,"
    done
    chn="${chn::-1}"

    # Perform a first DCS Measurement to check for possible authentication
    while true; do
        python3 $SOBASE/libDCS/psOperator.py -e $PSCFG -c $chn -d $INFLUXDBDCSCFG measure
        rv=$?
        if [ $rv -eq 0 ]; then
            # No authentication / No errors
            break

        elif [ $rv -eq 1 ]; then
            # Error othen than authentication failure
            exit 1;

        elif [ $rv -eq 2 ]; then

            # Authentication failure
            echo -n "Password: "
            read -s pwd
            export INFLUXDBPWD=$pwd
            echo ""
        fi
    done
fi

if [[ "$syncOption" == "" ]] && [[ "$localdbOpt" != "" ]]
then
    syncScript=$SOBASE/data/by_runnumber/$SO_RUNNUMBER/moveDCStoLocalDB.sh
    touch $syncScript
    echo "# Usage: " >> $syncScript
    echo "#     bash moveDCStoLocalDB.sh idb_to_ldb.json" >> $syncScript
    echo -e "\n" >> $syncScript
fi


nChips="$(jq -j --arg b ${module_id} '.modules."\($b)".chips | length' $MODULECFG)"
cfgdir=$SOBASE/configs/$RD53XS/$module_id
CNCT_FILE=$cfgdir/connectivity.json


#Reset or download(from LocalDB) config files
if [[ $resetConfig == 1 ]]; then
    echo -e "$(yellow [\ warn\ ][so]) -R option' reset remove current chip configs."
    echo -e "$(yellow [\ warn\ ][so]) Do you really reset the config in \"$(darkblue $cfgdir)\"?"
    unset answer
    while [ -z "${answer}" ]; do
        read -p "[y/n]: " answer
    done
    if [[ ${answer} == "n" ]]; then
        echo -e "OK. Try again without -R option."
        exit
    fi
    confGenVal=1 #create config by scanConsole
fi
if [ $createConfig == 1 ] || [ $resetConfig == 1 ] ; then
    rm -rf $cfgdir
    echo "[ info ][so] Creating configs for module \"$module_id\""

    cmd="$YARRBASE/bin/dbAccessor -D -n $module_id -p $cfgdir"
    $cmd
    retval=$?
    confGenVal=$retval

    if [ $retval == 0 ]; then
        cnt=0
        while [ $cnt -lt $nChips ]; do
            DBChipName=$(jq -r .chips[${cnt}].name $cfgdir/connectivity.json)
            LocalCONChipName=$(jq -r -j --argjson a ${cnt} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)
            LocalParChipName=$(jq -r -j --argjson a ${cnt} --arg b ${module_id} '.modules."\($b)".chips[$a].Parameter.Name' $MODULECFG)
            if [[ $DBChipName != $LocalCONChipName || $DBChipName != $LocalParChipName ]]; then
                echo -e "$(yellow [\ warn\ ][so]) The chip$((${cnt}+1)) name of \"$(darkblue $module_id)\" already exist in LocalDB and they are not same."
                echo -e "$(yellow [\ warn\ ][so]) The name in LocalDB is \"$(darkblue $DBChipName)\"  and the name you decided is \"$(darkblue $LocalCONChipName)\" and \"$(darkblue $LocalParChipName)\"."
                echo -e "$(yellow [\ warn\ ][so]) That difference may occur some problem in LocalDB."
                echo -e "$(yellow [\ warn\ ][so]) Do you change the name in \"$(darkblue $MODULECFG)\" to \"$(darkblue $DBChipName)\" in LocalDB?"
                sleep 1e-5
                unset answer
                while [ -z "${answer}" ]; do
                    read -p "[y/n]: " answer
                done
                if [[ ${answer} == "y" ]]; then
                    jq -j --argjson a ${cnt} --arg b ${module_id} --arg c ${DBChipName} '.modules."\($b)".chips[$a].connectivity.name|="\($c)"' $MODULECFG > CFGtmp
                    jq --argjson a ${cnt} --arg b ${module_id} --arg c ${DBChipName} '.modules."\($b)".chips[$a].Parameter.Name|="\($c)"' CFGtmp >$MODULECFG
                    rm -rf CFGtmp
                    echo -e "[ info ][so] Changed the name of chip from $(darkblue $LocalChipName) to $(darkblue $DBChipName) in the chip config file)"
                else
                    echo -e " [ info ][so] OK, the scan will run with $(darkblue $LocalChipName)."
                fi
            fi
            let ++cnt
        done
    else
        if [ "$qcOpt" = "-Q" ]; then
            echo "[ info ][so] Please use localdb's viewer to register the module from iTkPD"
            exit
        else
            rm -rf $cfgdir

            echo "[so] Do you want to register \"$module_id\" in localDB?"
            sleep 1e-5
            unset answer
            while [ -z "${answer}" ]; do
                read -p "[y/n]: " answer
            done
            if [[ ${answer} == "y" ]]; then

                echo "------------------------"

                echo -e "\n[ info ][so] Site information (\"$LDBSITCFG\")"
                cat $LDBSITCFG | jq .

                echo -e "\n[ info ][so] User information (\"$LDBUSRCFG\")"
                cat $LDBUSRCFG | jq .

                echo ""

                # Generate module information file
                # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
                modRegFile=$SOBASE/configs/ldb_module_registration.json
                touch $modRegFile

                echo "{}" > $modRegFile

                chipType=$(jq -j --arg a $module_id '.modules."\($a)".chipType' $MODULECFG)

                jq --arg a $module_id '."module" = {"serialNumber": "\($a)", "componentType": "Module"}' $modRegFile > sotmp && mv sotmp $modRegFile
                jq --arg a $chipType '."chipType" = "\($a)"' $modRegFile > sotmp && mv sotmp $modRegFile
                jq '."chips" = []' $modRegFile > sotmp && mv sotmp $modRegFile

                for (( i=0; i<$nChips; i++ ))
                do
                    chipId="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].Parameter.ChipId' $MODULECFG)"
                    chipName="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)"
                    
                    jq --argjson a ${i} --arg v $chipName '.chips[$a].serialNumber = "\($v)"' $modRegFile > sotmp && mv sotmp $modRegFile
                    jq --argjson a ${i}  '.chips[$a].componentType = "Front-end Chip"' $modRegFile > sotmp && mv sotmp $modRegFile
                    jq --argjson a ${i} --argjson b ${chipId}  '.chips[$a].chipId = $b' $modRegFile > sotmp && mv sotmp $modRegFile
                done

                echo "[ info ][so] Module registration file, (autogenerated from \"$MODULECFG\")"
                cat $modRegFile | jq .
                # < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < <


                echo -e "\n[ info ][so] Please make sure that these files (and the module registration file in particular) are correct before proceeding."
                echo "------------------------"

                echo "Are you ok with the settings above?"
                unset answer
                while [ -z "${answer}" ]; do
                    read -p "[y/n]: " answer
                done
                if [[ ${answer} == "y" ]]; then
                    retval=$($YARRBASE/bin/dbAccessor -C -c $modRegFile -u $LDBUSRCFG -i $LDBSITCFG)
                else
                    echo "[ info ][so] ok! Make the appropriate changes and come again"
                    exit
                fi
                #Retrieve cfg files from localdb
                cmd="$YARRBASE/bin/dbAccessor -D -n $module_id -p $cfgdir"
                $cmd
            else
                echo "Do you want to create temporary config files locally for \"$module_id\"?"

                unset answer
                while [ -z "${answer}" ]; do
                    read -p "[y/n]: " answer
                done
                if [[ ${answer} != "y" ]]; then
                    echo "[ info ][so] Oh, well, ok... Sorry, I somehow need those config files..."
                    exit
                fi
                conntmp="/tmp/$RANDOM"
                chiptype=$(jq -j --arg a $module_id '.modules."\($a)".chipType' $MODULECFG)
                conn="{\"chipType\":\"$chiptype\",\"chips\":[]}"

                cnt=0
                while [ $cnt -lt $nChips ]; do

                    # "name" in connectivity.json should to match the actual chipName
                    chipName=$(jq -j --arg a $module_id --argjson b $cnt '.modules."\($a)".chips[$b].serial_number' $MODULECFG)
                    if [ "$chipName" != "null" ]; then
                        conn=$(echo $conn | jq ".chips[$cnt] |= .+{\"name\":\"$chipName\"}")
                    fi

                    # these parameters will be modified later
                    conn=$(echo $conn | jq ".chips[$cnt] |= .+{\"tx\":0}")
                    conn=$(echo $conn | jq ".chips[$cnt] |= .+{\"rx\":0}")
                    conn=$(echo $conn | jq ".chips[$cnt] |= .+{\"enable\":1}")
                    conn=$(echo $conn | jq ".chips[$cnt] |= .+{\"locked\":0}")

                    cnt=$(( cnt + 1 ))

                done
                echo $conn | jq . > $conntmp
                if [ "$RD53XS" == "rd53b" ]; then
                    ls $YARRBASE/configs/defaults/default_rd53b.json
                    retval=$?
                    if [ $retval -ne 0 ]; then
                        cp $SOBASE/configs/Defaults/default_rd53b.json $YARRBASE/configs/defaults/default_rd53b.json
                    fi
                fi

                cmd="$YARRBASE/bin/dbAccessor -D -c $conntmp -p $cfgdir"
                $cmd
                retval=$?
                rm $conntmp
                if [ $retval -ne 0 ]; then
                    exit
                fi
            fi
        fi
    fi

    if [[ $resetConfig == 1 ]]; then
        #Retrieve cfg files from localdb
        if test $(find $cfgdir -type f | grep -v -E 'connectivity.json'  | wc -l) -ne 0; then
            find $cfgdir -type f | grep -v -E 'connectivity.json'  | xargs rm #-f
        fi
    fi

    # confirm whether the name of chips is unique
    for (( i=0; i<$nChips; i++ ))
    do
        #        echo ""
        #        echo "------------------------"
        # confirm whether the name of chips is unique
        chipName="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)"
        if test $(grep $chipName $MODULECFG | wc -l) -gt 2; then
            echo -e "$(red [\ error\ ][so]) The $chipName in $(darkblue $MODULECFG) is used for the other chip. Please change or fix it!"
            #exit
        fi
    done

    # for (( i=0; i<$nChips; i++ ))
    # do
    #     echo ""
    #     echo "------------------------"
    #     # create default chipConfig.json
    #     chipName="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)"
    #     echo -e "[ info ][so] Creating config file for chip \"$(green $chipName)\": $cfgdir/$chipName.json"
    #     $YARRBASE/bin/createConfig -t $RD53XL -n $chipName -o $cfgdir/$chipName.json #> /dev/null
    # done


    CRTL_FILE=$cfgdir/controller.json

    # Generate controller cfg file
    if [ ! -e $cfgdir/ctrlCfg.json ]; then
        #CRTL_FILE=$cfgdir/controller.json
        JudgeCONNTP="$(jq --arg b ${module_id} '.modules."\($b)".controllerType' $MODULECFG)"
        if [ $JudgeCONNTP = "null" ]; then
            echo -e "$(red [\ error\ ][so]) Couldn't find the $(darkblue controllerType\ key) in $(darkblue $MODULECFG). Please add it! You can check how to write in configs/so_modules.json at \"https://gitlab.cern.ch/YARR/utilities/scan-operator/-/blob/master/configs/so_modules.json\" ."
            exit
        fi
        ctrlType="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".controllerType' $MODULECFG)"
        if [ ! -e $YARRBASE/configs/controller/specCfg-$RD53XS-${ctrlType}.json ]; then
            echo -e "$(red [\ error\ ][so]) Couldn't find $(darkblue \"$YARRBASE/configs/controller/specCfg-$RD53XS-${ctrlType}.json\")! Could you check whether the file exists?"
            exit
        fi
        if [ -e $YARRBASE/configs/controller/specCfg-$RD53XS-${ctrlType}.json ]; then
            cp $YARRBASE/configs/controller/specCfg-$RD53XS-${ctrlType}.json $CRTL_FILE
            echo -e "[ info ][so] Generating $(darkblue \"controller.json\") with $(darkblue \"$YARRBASE/configs/controller/specCfg-$RD53XS-${ctrlType}.json\")"
            specNum=$(jq -j --arg b ${module_id} '.modules."\($b)".specNum' $MODULECFG)
            if [ "$specNum" != "null" ]; then
                jq --argjson v $specNum '.ctrlCfg.cfg.specNum = $v' $CRTL_FILE > currentValue && mv currentValue $CRTL_FILE
            fi
        fi
    else
        mv $cfgdir/ctrlCfg.json $cfgdir/controller.json
        echo -e "[ info ][so] Using $(darkblue $cfgdir/ctrlCfg.json) as controller.json!"
    fi
    if [ $confGenVal != 0 ]; then
        # Generate chip cfg files
        echo -e "[ info ][so] Creating config files for chips of ${module_id}."
        unbuffer $YARRBASE/bin/scanConsole -r $CRTL_FILE -c $CNCT_FILE
        for (( i=0; i<$nChips; i++ ))
        do
            if [ -e $cfgdir/JohnDoe_$i.json ]; then
                # echo ""
                # echo "------------------------"
                chipName="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)"
                jq --arg a $RD53XL --arg b ${chipName} --argjson c $i '. | ."\($a)".chips[$c].Parameter.Name |= "\($b)"' $cfgdir/JohnDoe_$i.json > $cfgdir/${chipName}.json
            fi
        done
    fi
fi

#Download config files from ITk PD
if [[ $PDOpt == 1 ]]; then
    DATE="$(date "+%Y%m%d%H%M%S")"

    CONBACK=$SOBASE/configs/$RD53XS/${module_id}_until_${DATE}_download
    unset answer
    echo -e "[ info ][so] To download config files from ITkDB, you need to enter ITkDB AccessCodes."
    while [ -z "${answer}" ]; do
        read -s -p "ITkDB AccessCode1: " answer
    done
    ITkCode1=$answer
    echo ""
    unset answer
    while [ -z "${answer}" ]; do
        read -s -p "ITkDB AccessCode2: " answer
    done
    ITkCode2=$answer
    echo ""
    if [ -e $cfgdir ]; then
        echo -e "$(yellow [\ warn\ ][so]) Already $(darkblue \"$cfgdir\") exists. Do you continue to download configs from ITk PD? [y/n]"
        unset answer
        while [ -z "${answer}" ]; do
            read -p "[y/n]: " answer
        done
        if [[ ${answer} != "y" ]]; then
            echo "[ info ][so] Ok, please try again without -P option."
            exit
        fi
        echo -e "[ info ][so] $(darkblue \"$cfgdir\") is moved to $(darkblue \"$CONBACK\") as a backup."
#mkdir $CONBACK
        cp -r $cfgdir $CONBACK
    else
        mkdir $cfgdir
    fi
    echo -e "[ info ][so] What stage do you download? Please enter the key correspond to the stage you want."
    echo -e "[ info ][so] MODULEWIREBONDING=W, MODULEPARYLEMASKING=M, MODULEPARYLENECOATING=C, MODULEPARYLENEUNMASKING=U, MODULEWIREBONDPROTECTION=P, MODULETHERMALCYCLING=T, MODULEBURNIN=B."
    echo -e "[ info ][so] Now, The files ITk PD has are"
    cmd="../ModuleConfigDownloader/get_module_conf.py -m $module_id -z -ac1 $ITkCode1 -ac2 $ITkCode2"
    $cmd
    unset answer
    while [ 1 ]; do
        read -p "[W/M/C/U/P/T/B]: " answer
        if [ $answer == "W" ]; then
            StageOpt="MODULEWIREBONDING"
            break
        elif [ $answer == "M" ]; then
            StageOpt="MODULEPARYLEMASKING"
            break
        elif [ $answer == "C" ]; then
            StageOpt="MODULEPARYLENECOATING"
            break
        elif [ $answer == "U" ]; then
            StageOpt="MODULEPARYLENEUNMASKING"
            break
        elif [ $answer == "P" ]; then
            StageOpt="MODULEWIREBONDPROTECTION"
            break
        elif [ $answer == "T" ]; then
            StageOpt="MODULETHERMALCYCLING"
            break
        elif [ $answer == "B" ]; then
            StageOpt="MODULEBURNIN"
            break
        else
            echo "[ info ][so] Entered key is invalid. Please try again!"
            continue
        fi
    done

    echo -e "[ info ][so] What temperature do you download?"
    unset answer
    while [ -z "${answer}" ]; do
        read -p "What temperature?( Please enter only number.): " answer
    done
    TempOpt=$answer
    cmd="../ModuleConfigDownloader/get_module_conf.py -m $module_id -s ${StageOpt} -t ${TempOpt}C -ac1 $ITkCode1 -ac2 $ITkCode2"
    #    echo -e "$cmd"
    $cmd
    cp $SOBASE/${module_id}/${StageOpt}/${TempOpt}C/* $SOBASE/configs/$RD53XS/${module_id}
    rm -r $module_id
    echo -e "[ info ][so] Downloaded to $(darkblue \"$cfgdir\")."
    echo -e "bye"
    exit
fi


# Check that cfg files are there
if [[ ! -d "$cfgdir" ]]|| [[ ! -f "$cfgdir/connectivity.json" ]]
then
    echo -e "$(red [\ error\ ][so]) Couldn't find config files for module \"$module_id\".\n"\
             "             Please run the S.O. again with '-c' to create them"
    exit
fi


echo ""
# Modify config files according to config.json
if [ $skipCfgCheck != 1 ] ; then
    for (( i=0; i<$nChips; i++ ))
    do

        # Chip config file
        chipName="$(jq -j --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity.name' $MODULECFG)"
        JSON_FILE=$cfgdir/$chipName.json

        echo -e "[ info ][so] Looking for cfg updates in chip $(( i+1 ))'s configs..."
        jq -r --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].GlobalConfig | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG |
        while IFS=" " read -r -a value; do
            key=${value[0]}
            val=${value[1]}

            currentValue=$(jq --arg k ${key} --arg r ${RD53XL} '."\($r)".GlobalConfig."\($k)"' $JSON_FILE)

            if [ "$currentValue" == "null" ];
            then
                jq --argjson v $val --arg k $key --arg r $RD53XL '."\($r)".GlobalConfig."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                echo -e "$(yellow [\ warn\ ][so]) Added key $(darkblue \"$key\") with value $(darkblue \"$val\") in the chip config file (GlobalConfig section)"

            elif [ "$currentValue" != $val ];
            then
                jq --argjson v $val --arg k $key --arg r $RD53XL '."\($r)".GlobalConfig."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                echo -e "[ info ][so] Changed $(darkblue \"$key\") from $(darkblue \"$currentValue\") to $(darkblue \"$val\") in the chip config file (GlobalConfig section)"
            fi
        done

        # Parameter section
        jq -r --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].Parameter | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG |
        while IFS=" " read -r -a value; do
            key=${value[0]}
            val=${value[1]}

            currentValue=$(jq -r --arg k ${key} --arg r $RD53XL '."\($r)".Parameter."\($k)"' $JSON_FILE)

            if [ "$currentValue" == "null" ];
            then
                jq --argjson v $val --arg k $key --arg r $RD53XL '."\($r)".Parameter."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                echo -e "$(yellow [\ warn\ ][so]) Added key $(darkblue \"$key\") with value $(darkblue \"$val\") in the chip config file (Parameter section)"

            elif [ "$currentValue" != "$val" ];
            then
                if ! [[ "$val" =~ ^[0-9]+$ ]] ; then
                    jq --arg v $val --arg k $key --arg r $RD53XL '."\($r)".Parameter."\($k)" = "\($v)"' $JSON_FILE > currentValue && mv currentValue $JSON_FILE

                else
                    jq --argjson v $val --arg k $key --arg r $RD53XL '."\($r)".Parameter."\($k)" = $v' $JSON_FILE > currentValue && mv currentValue $JSON_FILE
                fi

                echo -e "[ info ][so] Changed $(darkblue \"$key\") from $(darkblue \"$currentValue\") to $(darkblue \"$val\") in the chip config file (Parameter section)"
            fi
        done
        # Connectivity file:
        jq -e --argjson a ${i} --arg v $chipName '.chips[$a].name = "\($v)"' $CNCT_FILE > currentValue && mv currentValue $CNCT_FILE
        jq -e --argjson a ${i} --arg v $JSON_FILE '.chips[$a].config = "\($v)"' $CNCT_FILE > currentValue && mv currentValue $CNCT_FILE
        jq -r --argjson a ${i} --arg b ${module_id} '.modules."\($b)".chips[$a].connectivity | keys[] as $k | "\($k) \(.[$k])"' $MODULECFG |
        while IFS=" " read -r -a value; do
            key=${value[0]}
            val=${value[1]}

            currentValue=$(jq -r --argjson a ${i} --arg k ${key} '.chips[$a]."\($k)"' $CNCT_FILE)

            if [ "$currentValue" == "null" ];
            then
                jq --argjson a ${i} --argjson v $val --arg k $key '.chips[$a]."\($k)" = $v' $CNCT_FILE > currentValue && mv currentValue $CNCT_FILE
                echo -e "$(yellow [\ warn\ ][so]) Added key $(darkblue \"$key\") with value $(darkblue \"$val\") in the connectivity file (chip $i)"

            elif [ "$currentValue" != "$val" ];
            then
                if ! [[ "$val" =~ ^[0-9]+$ ]] ; then
                    jq --argjson a ${i} --arg v $val --arg k $key '.chips[$a]."\($k)" = "\($v)"' $CNCT_FILE > currentValue && mv currentValue $CNCT_FILE
                else
                    jq --argjson a ${i} --argjson v $val --arg k $key '.chips[$a]."\($k)" = $v' $CNCT_FILE > currentValue && mv currentValue $CNCT_FILE
                fi

                echo -e "[ info ][so] Changed $(darkblue \"$key\") from $(darkblue \"$currentValue\") to $(darkblue \"$val\") in the connectivity file (chip $i)"
            fi
        done


        # Create an entry with the module serial number (to associate the config
        # files to in localdb)
        jq '. += {"module"}' $CNCT_FILE > sotmp && mv sotmp $CNCT_FILE
        jq --arg a ${module_id} '.module += {"serialNumber": "\($a)"}' $CNCT_FILE > sotmp && mv sotmp $CNCT_FILE
        jq '.module += {"componentType": "module"}' $CNCT_FILE > sotmp && mv sotmp $CNCT_FILE
        echo ""
    done
fi


echo "[ info ][so] Current connectivity config:"
echo "------------------------"
cat $cfgdir/connectivity.json | jq '.' -C
echo "------------------------"


if [ $justConfigure == 1 ] ;then
    echo -e "[ info ][so] Configuration updated."
    exit 0
fi

if [ $dcsOnOff == 1 ] ;then
    echo -e "\n[ info ][so] Turning ON the PS..."

    nChannels="$(jq -j '.channels_to_be_used_by_the_scanoperator | length' $PSCFG)"
    for (( i=0; i<$nChannels; i++ ))
    do
        chn="$(jq -j ".channels_to_be_used_by_the_scanoperator[$i].name" $PSCFG)"
        python3 $SOBASE/libDCS/psOperator.py -e $PSCFG -c $chn -d $INFLUXDBDCSCFG power-on
        if [ $? -ne 0 ]; then exit 1; fi
    done
fi

if [ $ApplyConfig == 1 ] ;then
    ctrlFile="$SOBASE/configs/$RD53XS/$module_id/controller.json"
    cnctFile="$SOBASE/configs/$RD53XS/$module_id/connectivity.json"
    comm="$YARRBASE/bin/scanConsole -r $ctrlFile -c $cnctFile -o $SOBASE/data/$SO_RUNNUMBER -n 1" #> /dev/null 2>1
    $comm
else
    echo -e "\n[ info ][so] Running the sequence of scans"
    echo -e "\n[ info ][so] Running scan list is ${scan_list}"
    
    nScans="$(jq -j --arg a $scan_list '."\($a)" | length' $SCANCFG)"
    for i in $(seq 1 $nScans); do
	
	if [[ "$dcsMonitor" == "1" ]]; then
            waitForItToFinish "read_dcs_backgr"
            dcsOpt="-d"
	fi
	
	if [[ $envMonitor == 1 ]]; then
            # check if T and H are within the working interval before every scan
            python3 $SOBASE/libDCS/getFromInfluxDB.py -j $INFLUXDBENVCFG
            retval=$?
	    if [ $retval -ne 0 ]; then
		wrapAndExit
		break
      fi
  fi

  obj="$(jq -j  -c --argjson a ${i} --arg b $scan_list '."\($b)"[$a-1]' $SCANCFG)"
  echo $obj

	comm="$SOBASE/scanLauncher.sh -m ${module_id} -s $obj ${dcsOpt} ${localdbOpt} ${qcOpt} ${syncOption}"
	echo "$comm"
  obj="$(echo -e "${obj}" | sed -e "s/\ /thisIsSpace/g")"
	comm="$SOBASE/scanLauncher.sh -m ${module_id} -s $obj ${dcsOpt} ${localdbOpt} ${qcOpt} ${syncOption}"
	echo -e "[ info ] |\n[ info ] | - $(echo $obj | jq -r '.[0]')\n[ info ] |"
	if [ $quietMode -gt 0 ];
	then
            output=$($comm 2>&1 | awk '/ Mean /{print $0}')
            retval=$?
            if [ "$output" != "" ];then
		while IFS= read -r line ; do echo -e "[ info ] |     -> ${line##*\] }"; done <<< "$output"
            else
		echo -e "[ info ] |     -> done"
            fi
	else
            $comm
            retval=$?
	fi
	if [ $retval -ne 0 ]; then
            wrapAndExit
            break
	fi
    done


    if [ $dcsOnOff == 1 ]; then
	echo -e "\n[ info ][so] Turning OFF the PS..."
	
	nChannels="$(jq -j '.channels_to_be_used_by_the_scanoperator | length' $PSCFG)"
	for (( i=0; i<$nChannels; i++ ))
	do
            chn="$(jq -j ".channels_to_be_used_by_the_scanoperator[$i].name" $PSCFG)"
            python3 $SOBASE/libDCS/psOperator.py -e $PSCFG -c $chn -d $INFLUXDBDCSCFG power-off
            if [ $? -ne 0 ]; then exit 1; fi
	done
    fi
    
    if [[ "$dcsMonitor" != "" ]]; then
	#wait for the dcs monitor script to finish
	waitForItToFinish "read_dcs_backgr"
    fi
    
    if [[ "$syncOption" == "" ]] && [[ "$localdbOpt" != "" ]]
    then
	
	cat << EOF
[ info ][so] If you want to upload to localDB the DCS data associated
             to all the scans you have just run (assuming you have it 
             stored in InfluxDB), you can do it in one command:

                 bash $SOBASE/data/by_runnumber/$SO_RUNNUMBER/moveDCStoLocalDB.sh path/to/idb_to_ldb.json
EOF
	
    fi
fi

elp=$(echo "$(date +%s.%N) - $START_SO" | bc)
echo -ne "total_so     $elp\n" >> $TIMEFILE

echo -e "[ info ][so] All done"

} | tee $SOBASE/ScanTestLog
cat $SOBASE/ScanTestLog | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})*)?m//g" > $SOBASE/ScanTestLogWithoutColor
mv $SOBASE/ScanTestLog $SOBASE/data/by_runnumber/$SO_RUNNUMBER/ScanMessageLog
mv $SOBASE/ScanTestLogWithoutColor $SOBASE/data/by_runnumber/$SO_RUNNUMBER/ScanMessageLogWithoutColor
